﻿using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Http.Owin;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Bartek.IdentitiDataAccess.Impl.EF.AuthenticationMangerFactory
{
    public interface IAuthenticationMangerFactory
    {
        IAuthenticationManager CreateAuthManager();
       // IOwinContext GetOwinContext();
    }

    public class AuthenticationMangerFactory : IAuthenticationMangerFactory
    {
        public IAuthenticationManager CreateAuthManager()
        {
           return HttpContext.Current.GetOwinContext().Authentication;
        }
        public static IAuthenticationManager CreateAuthManagerStatic()
        {
            return HttpContext.Current.GetOwinContext().Authentication;
        }

        public static IOwinContext GetOwinContext()
        {
            return HttpContext.Current.GetOwinContext() ?? null;
        }
              
    }
}

﻿using Bartek.DataAccess.Contract.IUserManagers;
using Bartek.DataAccess.Contract.UserManagers;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.DataAccess.Contract.UserEnums;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Bartek.IdentitiDataAccess.Impl.EF.Helpers;
using Microsoft.Owin;
using System.Net;
using Bartek.Entities.Contracts;

namespace Bartek.IdentitiDataAccess.Impl.EF.UserManagers
{
    public class ApplicationSignInManager : SignInManager<MyIdentityUser, Guid>, ISignInManager<IMyIdentityUser>
    {
        public ApplicationSignInManager(IUserManager<IMyIdentityUser> userManager, IAuthenticationManager authenticationManager)
            : base(userManager as ApplicationUserManager, authenticationManager)
        {
        }
        public override Task<ClaimsIdentity> CreateUserIdentityAsync(MyIdentityUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(ApplicationUserManager appUserManager, IAuthenticationManager authManager)
        {         
            return new ApplicationSignInManager(appUserManager, authManager);
        }

     
        public async Task<MySignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent)
        {
            return SignInStatusConverter.Convert(await (this as SignInManager<MyIdentityUser, Guid>).PasswordSignInAsync(userName, password, isPersistent, false));
        }

        async Task ISignInManager<IMyIdentityUser>.SignInAsync(IMyIdentityUser user, bool isPersistent, bool rememberBrowser)
        {
            await (this as ApplicationSignInManager).SignInAsync(user.ConvertTo<MyIdentityUser>(), isPersistent, rememberBrowser);
        }

        
    }
}

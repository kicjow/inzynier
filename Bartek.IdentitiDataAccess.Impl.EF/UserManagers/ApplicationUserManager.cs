﻿using Bartek.DataAccess.Impl.Ef.Context;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.DataAccess.Contract.UserManagers;
using Bartek.DataAccess.Contract.Misc;
using Bartek.IdentitiDataAccess.Impl.EF.Misc;
using Bartek.Entities.Contracts;
using System.Security.Claims;

namespace Bartek.IdentitiDataAccess.Impl.EF.UserManagers
{
    public class ApplicationUserManager : UserManager<MyIdentityUser, Guid>, IUserManager<IMyIdentityUser>
    {
        UserManager<MyIdentityUser, Guid> _manager;
        public ApplicationUserManager(IUserStore<MyIdentityUser, Guid> store)
            : base(store)
        {
            _manager = (this as UserManager<MyIdentityUser, Guid>);
        }


        public ApplicationUserManager(DbContext dbContext) :
            base(new UserStore<MyIdentityUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>(dbContext))
        {
            _manager = (this as UserManager<MyIdentityUser, Guid>);

        }

        public ApplicationUserManager(IOwinContext context) :
            base(new UserStore<MyIdentityUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>(context.Get<MyIdentityDbContext>()))
        {
            _manager = (this as UserManager<MyIdentityUser, Guid>);
        }

        public static IUserManager<IMyIdentityUser> Create(DbContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<MyIdentityUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>(context));
            ConfigureManager(manager);
            return manager;
        }

        public static IUserManager<IMyIdentityUser> Create(IOwinContext context)
        {
            var manager = new ApplicationUserManager(
                new UserStore<MyIdentityUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>(context.Get<MyIdentityDbContext>()));
            ConfigureManager(manager);

            return manager;
        }

        //public static IUserManager<IMyIdentityUser> Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        //{
        //    var manager = ApplicationUserManager.Create(context) as ApplicationUserManager;


        //    var dataProtectionProvider = options.DataProtectionProvider;
        //    if (dataProtectionProvider != null)
        //    {
        //        if (manager != null)
        //        {
        //            manager.UserTokenProvider = new DataProtectorTokenProvider<MyIdentityUser, Guid>(dataProtectionProvider.Create("ASP.NET Identity"));
        //        }
        //    }
        //    return manager;
        //}

        private static void ConfigureManager(ApplicationUserManager manager)
        {

            manager.UserValidator = new UserValidator<MyIdentityUser, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 50;
        }


        public async Task<IMyIdentityResult> ConfirmEmailAsync(IMyIdentityUser user, string token)
        {
            var baseIdent = await this.ConfirmEmailAsync(user.ID, token);
            return new MyIdentityResult(baseIdent);
        }

        public async Task<ClaimsIdentity> CreateIdentityAsync(IMyIdentityUser user, string authType)
        {

            MyIdentityUser identUser = user.ConvertTo<MyIdentityUser>();
            return await _manager.CreateIdentityAsync(identUser, authType);
        }

        public IMyIdentityUser FindById(Guid key)
        {
            return _manager.FindById(key);
        }

        public async Task<IMyIdentityUser> FindByNameAsync(IMyIdentityUser user)
        {

            MyIdentityUser identUser = user.ConvertTo<MyIdentityUser>();
            return await _manager.FindByNameAsync(identUser.UserName);
        }

        public async Task<bool> IsEmailConfirmedAsync(IMyIdentityUser user)
        {

            MyIdentityUser identUser = user.ConvertTo<MyIdentityUser>();
            return await _manager.IsEmailConfirmedAsync(user.ID);
        }

        public async Task<IMyIdentityResult> ResetPasswordAsync(IMyIdentityUser user, string token, string newPassword)
        {

            MyIdentityUser identUser = user.ConvertTo<MyIdentityUser>();
            return new MyIdentityResult(await _manager.ResetPasswordAsync(user.ID, token, newPassword));
        }

        async Task<IMyIdentityResult> IUserManager<IMyIdentityUser>.CreateAsync(IMyIdentityUser user, string password)
        {
            try
            {
                MyIdentityUser identUser = user.ConvertTo<MyIdentityUser>();
                return new MyIdentityResult(await _manager.CreateAsync(identUser, password));

            }
            catch (Exception e)
            {
                
                throw;
            }
            //MyIdentityUser identUser = user.ConvertTo<MyIdentityUser>();
            //return new MyIdentityResult(await _manager.CreateAsync(identUser, password));
        }

        async Task<IMyIdentityUser> IUserManager<IMyIdentityUser>.FindByIdAsync(Guid id)
        {
            var user = await _manager.FindByIdAsync(id);
            return user;
        }

        public async Task<IMyIdentityResult> DeleteAsync(IMyIdentityUser user)
        {
            return new MyIdentityResult(await _manager.DeleteAsync(user.ConvertTo<MyIdentityUser>()));
        }

        public async Task<IMyIdentityResult> AddClaimAsync(IMyIdentityUser user, Claim claim)
        {
            return new MyIdentityResult(await _manager.AddClaimAsync(user.ID, claim));
        }

        public async Task<IMyIdentityResult> RemoveClaimAsync(IMyIdentityUser user, Claim claim)
        {
            return new MyIdentityResult(await _manager.RemoveClaimAsync(user.ID, claim));
        }

        public async Task<IMyIdentityResult> AddRoleAsync(IMyIdentityUser user, string role)
        {
            return new MyIdentityResult(await _manager.AddToRoleAsync(user.ID, role));
        }

        public async Task<IMyIdentityResult> RemoveRoleAsync(IMyIdentityUser user, string role)
        {
            return new MyIdentityResult(await _manager.RemoveFromRoleAsync(user.ID, role));
        }
    }

}

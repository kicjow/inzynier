﻿using Bartek.DataAccess.Contract.UserManagers;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.Contracts;
using Bartek.IdentitiDataAccess.Impl.EF.UserManagers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.IdentitiDataAccess.Impl.EF
{
    public class MyIdentityUser : IdentityUser<Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>, IMyIdentityUser, IBaseObject
    {
        public virtual AppUser AppUser { get; set; }

        public MyIdentityUser()
        {
            ID = Guid.NewGuid();
        }
        // [NotMapped]
        public Guid ID
        {
            get { return this.Id; }
            set { this.Id = value; }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            var userIdentity = await (manager).CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

       

        public T ConvertTo<T>() where T : class, IMyIdentityUser
        {
            var result = this as T;
            if (result == null)
            {
                throw new InvalidCastException($"This user implementation is diffrent from {nameof(MyIdentityUser)}");
            }

            return result;
        }
    }

    public class GuidRole : IdentityRole<Guid, GuidUserRole>
    {
        public GuidRole()
        {
            Id = Guid.NewGuid();
        }
        public GuidRole(string name) : this() { Name = name; }
    }

    public class GuidUserRole : IdentityUserRole<Guid>
    {
      
    }
    public class GuidUserClaim : IdentityUserClaim<Guid>
    {
     
    }
    public class GuidUserLogin : IdentityUserLogin<Guid>
    {
        
    }
}

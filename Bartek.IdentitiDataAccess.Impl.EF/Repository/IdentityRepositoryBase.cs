﻿using Bartek.Core.CastleCore;
using Bartek.DataAccess.Impl.Ef.Context;
using Bartek.DataAccess.Impl.Ef.ContextRepository;
using Bartek.Entities.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Impl.Ef.Repository
{
    public abstract class IdentityRepositoryBase<T> where T : BaseObject
    {
        protected IdenityDbContext Context;
        protected DbSet DbSet;

        protected IdentityRepositoryBase()
        {
            Context = CastleCore.Resolve<IIdentityRepositoryContext>().IdenityDbContext;
            DbSet = Context.GetDbSet<T>();
        }

    }
}

﻿using Bartek.DataAccess.Contract.IRepository;
using Bartek.Entities.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Impl.Ef.Repository
{
    public class IdentityRepository<T> : IdentityRepositoryBase<T>, IRepository<T> where T : BaseObject
    {

        //TODO: DODAWANIE UZYTKOWNIKA KTORY DOKONAL ZMIANY LUB KREACJI OBIEKTU
        public IdentityRepository()
            : base()//string connectionString): base(connectionString)
        { }

        /// <summary>
        /// Dodaje obiekt do aktualnego DBsetu. Jeśli obiekt o takim Guidzie istnieje modyfikuje go.
        /// </summary>
        /// <param name="obiekt"></param>
        public void Add(T obiekt)
        {

            var entry = (T)DbSet.Find(obiekt.ID);
            if (entry != null)
            {
                //entry.CzasOstatniejModyfikacji = DateTime.Now;
                //entry.PrzepiszDane(obiekt);
                Context.Entry<T>(entry).CurrentValues.SetValues(obiekt);
            }
            else
            {
                //obiekt.CzasOstatniejModyfikacji = DateTime.Now;
                DbSet.Add(obiekt);
            }

            this.Context.SaveChanges();
        }
        /// <summary>
        /// Pobiera wszystkie obiekty tego typu z bazy.
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetAll()
        {
            return DbSet.OfType<T>();
        }

        /// <summary>
        /// Usuwa i zwraca istniejęcy obiekt. Zwraca null w innym przypadku.
        /// </summary>
        /// <param name="oid"></param>
        public T Delete(Guid oid)
        {
            var entry = (T)DbSet.Find(oid);
            if (entry != null)
            {

                DbSet.Remove(entry);
                //entry.CzasOstatniejModyfikacji = DateTime.Now;
                Context.SaveChanges();
            }
            return entry;

        }

        /// <summary>
        /// Usuwa i zwraca PIERWSZY obiekt spełniający dany predykat. Zwraca w null w innym przypadku SUPER WAZNE! można używać query tylko na typach prymitywnych, nie da sie zrobić Delete(x=> x == user) 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public T Delete(System.Linq.Expressions.Expression<Func<T, bool>> query)
        {
            var allGivenTypes = DbSet.OfType<T>();
            var filteredTypes = allGivenTypes.Where(query);
            var objectToDelete = filteredTypes.FirstOrDefault();
            if (objectToDelete != null)
            {
                DbSet.Remove(objectToDelete);
                Context.SaveChanges();
                // objectToDelete.CzasOstatniejModyfikacji = DateTime.Now;
            }
            return objectToDelete;

        }

        /// <summary>
        ///  Usuwa i zwraca IQueryable wszystkich obiektów spełniających dany predykat. Zwraca pustą kolekcje w innym przypadku.  SUPER WAZNE! można używać query tylko na typach prymitywnych, nie da sie zrobić Delete(x=> x == user) 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IQueryable<T> DeleteAll(Expression<Func<T, bool>> query)
        {
            var allGivenTypes = DbSet.OfType<T>();
            var filteredTypes = allGivenTypes.Where(query).ToList();
            var objectsToDelete = filteredTypes;
            if (objectsToDelete.Count() != 0)
            {
                DbSet.RemoveRange(objectsToDelete);
                Context.SaveChanges();
                // objectsToDelete.ForEach(x => x.CzasOstatniejModyfikacji = DateTime.Now);
            }
            return objectsToDelete.AsQueryable();

        }

        /// <summary>
        /// Zwraca IQueryable wszystkich obiektów spełniający dany predykat.
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public IQueryable<T> FindAll(Expression<Func<T, bool>> func)
        {
            var allGivenTypes = DbSet.OfType<T>();
            var filteredTypes = allGivenTypes.Where(func);
            return filteredTypes;

        }

        /// <summary>
        /// Zwraca pierwszy obiekt spełniający dany predykat
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public T GetObject(Expression<Func<T, bool>> query)
        {
            var allGivenTypes = DbSet.OfType<T>();
            var filteredTypes = allGivenTypes.Where(query);
            return filteredTypes.FirstOrDefault();
        }

        /// <summary>
        /// Dodaje kolekcje obiektów
        /// </summary>
        /// <param name="collection"></param>
        public void AddRange(IEnumerable<T> collection)
        {
            foreach (var entry in collection)
            {
                Add(entry);
            }
        }
    }


}

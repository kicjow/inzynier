﻿using Bartek.Core.Helpers;
using Bartek.DataAccess.Impl.Ef.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Impl.Ef.ContextRepository
{
    public class IdentityRepositoryContext : IIdentityRepositoryContext
    {
        private IdenityDbContext _IdenityDbContext;
        public IdenityDbContext IdenityDbContext
        {
            get
            {
                if (IdenityDbContext == null)
                {
                    string connectionString = DbHelper.GetConnectionString();
                    _IdenityDbContext = new IdenityDbContext(connectionString);
                }

                return IdenityDbContext;
            }
        }
    }
}

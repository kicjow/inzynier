﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.DataAccess.Contract.Misc;
using Microsoft.AspNet.Identity;

namespace Bartek.IdentitiDataAccess.Impl.EF.Misc
{
    public class MyIdentityResult : IMyIdentityResult
    {
        private IdentityResult _msIdentity;

        public MyIdentityResult(IdentityResult msIdentity)
        {
            this._msIdentity = msIdentity;
        }

        public IEnumerable<string> Errors
        {
            get
            {
                return _msIdentity.Errors;
            }
        }

        public bool Succeeded
        {
            get
            {
                return _msIdentity.Succeeded;
            }
        }

        public IMyIdentityResult Success
        {
            get
            {
                var baseIdentity = IdentityResult.Success;
                return new MyIdentityResult(baseIdentity);
            }
        }

        public IMyIdentityResult Failed(params string[] errors)
        {
            var baseIdentity = IdentityResult.Failed(errors);
            return new MyIdentityResult(baseIdentity);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.IdentitiDataAccess.Impl.EF.Configurations
{
    public class MyIdentityUserConfiguration : EntityTypeConfiguration<MyIdentityUser>
    {
        public MyIdentityUserConfiguration()
        {
            //  HasKey(x => x.Id);
            Ignore(x => x.ID);
        }
    }

    #region kłócą się z EF identityContext - pewnie ma już swoje wbudowane a te są dla niego za dużo
    public class GuidUserRoleConfiguration : EntityTypeConfiguration<GuidUserRole>
    {
        public GuidUserRoleConfiguration()
        {
            this.ToTable("dbo.AspNetUserRoles");
            HasKey(x => new { x.UserId, x.RoleId });
        }
    }
    public class GuidUserClaimConfiguration : EntityTypeConfiguration<GuidUserClaim>
    {
        public GuidUserClaimConfiguration()
        {
            HasKey(x => x.Id);
            this.ToTable("dbo.AspNetUserClaims");
        }
    }
    public class GuidUserLoginConfiguration : EntityTypeConfiguration<GuidUserLogin>
    {
        public GuidUserLoginConfiguration()
        {
            HasKey(x => new { x.UserId, x.ProviderKey, x.LoginProvider });
            this.ToTable("dbo.AspNetUserLogins");
        }
    }
    public class GuidRoleConfiguration : EntityTypeConfiguration<GuidRole>
    {
        public GuidRoleConfiguration()
        {
            HasKey(x => x.Id);
            this.ToTable("dbo.AspNetRoles");
        }
    }

    #endregion
}

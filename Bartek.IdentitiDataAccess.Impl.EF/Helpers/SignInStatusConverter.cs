﻿using Bartek.DataAccess.Contract.UserEnums;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.IdentitiDataAccess.Impl.EF.Helpers
{
    public static class SignInStatusConverter
    {
        public static MySignInStatus Convert(SignInStatus source)
        {
            var result = (MySignInStatus)((int)source);
            return result;
        }
    }
}

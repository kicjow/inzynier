﻿using Bartek.Core.Helpers;
using Bartek.Entities.BusinessObjects;
using Bartek.IdentitiDataAccess.Impl.EF;
using Bartek.IdentitiDataAccess.Impl.EF.Configurations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.Confiugarations.EF.Configurations;

namespace Bartek.DataAccess.Impl.Ef.Context
{
    public class MyIdentityDbContext : IdentityDbContext<MyIdentityUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {

        public MyIdentityDbContext(string connectionString) : base(connectionString)
        {
            //  Database.SetInitializer(new IdentityContextInitializer());
            //  bool czyUtworzyl = Database.CreateIfNotExists();

            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;

        }

        private object ThrowNewArgumentExeption()
        {
            throw new ArgumentException("Zły typ został przekazy do GetDbSet<T>, ApplicationContext");
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //modelBuilder.Configurations.Add(new GuidUserRoleConfiguration());
            //modelBuilder.Configurations.Add(new GuidUserClaimConfiguration());
            //modelBuilder.Configurations.Add(new GuidUserLoginConfiguration());
            //modelBuilder.Configurations.Add(new GuidRoleConfiguration());
            

            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new MyIdentityUserConfiguration());
            modelBuilder.Configurations.Add(new AppUserConfiguration());


        }
    }

    public class GuidUserStore : UserStore<MyIdentityUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public GuidUserStore(DbContext context)
            : base(context)
        {
        }
    }
    public class GuidRoleStore : RoleStore<GuidRole, Guid, GuidUserRole>
    {
        public GuidRoleStore(DbContext context)
            : base(context)
        {
        }
    }

    public class MyContextFactory : IDbContextFactory<MyIdentityDbContext>
    {
        public MyIdentityDbContext Create()
        {
            var connString = DbHelper.ConnectionString; //"Data Source=(local); Initial Catalog=Bartek-tests; Integrated Security=SSPI;";//ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            return new MyIdentityDbContext(connString);
        }
    }

}

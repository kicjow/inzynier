﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Impl.Ef.Context
{
    public class IdentityContextInitializer : CreateDatabaseIfNotExists<MyIdentityDbContext>
    {
        public override void InitializeDatabase(MyIdentityDbContext context)
        {
            
            context.Configuration.LazyLoadingEnabled = true;
            context.Configuration.ProxyCreationEnabled = true;

            base.InitializeDatabase(context);

        }
        protected override void Seed(MyIdentityDbContext context)
        {
            base.Seed(context);
        }
    }

    public sealed class Configuration : DbMigrationsConfiguration<MyIdentityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}

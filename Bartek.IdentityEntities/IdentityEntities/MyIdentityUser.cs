﻿using Bartek.Entities.BusinessObjects;
using Bartek.Entities.Contracts;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.IdentityEntities.IdentityEntities
{
    public class MyIdentityUser : IdentityUser<Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>, IMyIdentityUser, IBaseObject
    {
        public AppUser AppUser { get; set; }
        public MyIdentityUser()
        {
            ID = Guid.NewGuid();
        }
        public Guid ID
        {
            get { return this.Id; }
            set { this.Id = value; }
        }

        public async Task<GuidUserClaim> GenerateUserIdentityAsync(UserManager<MyIdentityUser, Guid> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class GuidRole : IdentityRole<Guid, GuidUserRole>
    {
        public GuidRole()
        {
            Id = Guid.NewGuid();
        }
        public GuidRole(string name) : this() { Name = name; }
    }

    public class GuidUserRole : IdentityUserRole<Guid> { }
    public class GuidUserClaim : IdentityUserClaim<Guid> { }
    public class GuidUserLogin : IdentityUserLogin<Guid> { }
}

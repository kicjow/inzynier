﻿using System;
using System.Data.Entity;
using System.IO;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using Bartek.IdentitiDataAccess.Impl.EF.UserManagers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Core;
using NUnit.Framework;
using System.Web.Mvc;
using Bartek.Core.CastleCore;
using Bartek.Core.Helpers;
using Bartek.DataAccess.Contract.IRepository;
using Bartek.DataAccess.Contract.IUserManagers;
using Bartek.DataAccess.Contract.UserManagers;
using Bartek.DataAccess.Impl.Ef.Context;
using Bartek.DataAccess.Impl.Ef.ContextRepository;
using Bartek.DataAccess.Impl.Ef.Repository;
using Bartek.Entities.Contracts;
using Bartek.IdentitiDataAccess.Impl.EF;
using Bartek.IdentitiDataAccess.Impl.EF.AuthenticationMangerFactory;
using Castle.MicroKernel.Registration;
using NU = NUnit.Framework;
using MSTests = Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bartek.Tests.IdentityTests
{
    [TestFixture]
    public class IdentityUserTests
    {
        [OneTimeSetUp] //raz tylko na poczatku wszystkich testów
        public void Init()
        {
            var connectionString = DbHelper.ConnectionString;
            MyCore.Container.Register(Component.For<IMyIdentityUser>().ImplementedBy<MyIdentityUser>());

            MyCore.Container.Register(Component.For<IAuthenticationManager>().Instance(new Mock<IAuthenticationManager>().Object));
            // MyCore.Container.Register(Component.For<IOwinContext>().UsingFactoryMethod(AuthenticationMangerFactory.GetOwinContext));

            MyCore.Container.Register(Component.For<IUserManager<IMyIdentityUser>>().ImplementedBy<ApplicationUserManager>()
              .DependsOn(Dependency.OnValue(typeof(DbContext), new MyIdentityDbContext(connectionString))));
            MyCore.Container.Register(Component.For<ISignInManager<IMyIdentityUser>>().ImplementedBy<ApplicationSignInManager>());
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            // tylko raz po wykonaniu wszystkich testów
        }

        [SetUp] // wykonywany przed kazdym testem
        public void SetUpEachTest()
        {


        }
        [TearDown] //wykonywany PO kazdym tescie
        public void TearDown()
        {
            //TODO
        }

        [Test]
        public async Task CanRegisterNewUser()
        {
            // Arrange
            //nie używać twardych implementacji - gdy coś się zepsuje to nie będzie wiadomo co dokładniej
            HttpContext.Current = CreateHttpContext(userLoggedIn: true);
            var userManager = MyCore.Resolve<IUserManager<IMyIdentityUser>>();
            var signInManager = MyCore.Resolve<ISignInManager<IMyIdentityUser>>();

            var user = MyCore.Resolve<IMyIdentityUser>();
            var random = new Random().Next();
            user.UserName = "useName" + random;
            user.Email = random + "email@email.com";
            var password = "password";

            //synchroniczne metody muszą być dodane

            var result = await userManager.CreateAsync(user, password);

            var userFromDb = await MyCore.Resolve<IUserManager<IMyIdentityUser>>().FindByIdAsync(user.ID);

            NU.Assert.IsNotNull(userFromDb);
            NU.Assert.AreEqual(userFromDb.Email,user.Email);
            NU.Assert.IsTrue(result.Succeeded);
        }
        [Test]
        public async Task CanDeleteUser()
        {
            HttpContext.Current = CreateHttpContext(userLoggedIn: true);
            var userManager = MyCore.Resolve<IUserManager<IMyIdentityUser>>();
            var signInManager = MyCore.Resolve<ISignInManager<IMyIdentityUser>>();
            var user = MyCore.Resolve<IMyIdentityUser>();
            var random = new Random().Next();
            user.UserName = "useName" + random;
            user.Email = random + "email@email.com";
            var password = "password";

            var addResult = await userManager.CreateAsync(user, password);

            NU.Assert.IsTrue(addResult.Succeeded);

            var deleteResult = await userManager.DeleteAsync(user);

            NU.Assert.IsTrue(deleteResult.Succeeded);
        }


        private static HttpContext CreateHttpContext(bool userLoggedIn)
        {
            var httpContext = new HttpContext(
                new HttpRequest(string.Empty, "http://sample.com", string.Empty),
                new HttpResponse(new StringWriter())
            )
            {
                User = userLoggedIn
                    ? new GenericPrincipal(new GenericIdentity("userName"), new string[0])
                    : new GenericPrincipal(new GenericIdentity(string.Empty), new string[0])
            };

            return httpContext;
        }
    }
}

﻿using Bartek.Core.CastleCore;
using Bartek.DataAccess.Contract.IRepository;
using Bartek.DataAccess.Impl.Ef.ContextRepository;
using Bartek.DataAccess.Impl.Ef.Repository;
using Bartek.Entities.BusinessObjects;
using Castle.MicroKernel.Registration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjectsPolish;
using Bartek.MVC.Infrastructure;

namespace Bartek.Tests.RepositoryTests
{
    [TestFixture]
    public class AppUserTests
    {

        [OneTimeSetUp] //raz tylko na poczatku wszystkich testów
        public void Init()
        {
            MyCore.Container.Register(Component.For<IRepositoryContext>().ImplementedBy<RepositoryContextImpl>().LifestyleSingleton());
            MyCore.Container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)));
            RootRegisterHelper.RegisterRelation();
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            // tylko raz po wykonaniu wszystkich testów
        }

        [SetUp] // wykonywany przed kazdym testem
        public void SetUpEachTest()
        {


        }
        [TearDown] //wykonywany PO kazdym tescie
        public void TearDown()
        {
            //TODO
        }

        [Test]
        public void CanAddNewUser()
        {
            string firstName = "first name";
            string lastname = "last name";
            var nick = "nick";
            var newUser = new AppUser();

           
            newUser.Nick = nick;

            var userRepository = MyCore.Resolve<IRepository<AppUser>>();
            userRepository.Add(newUser);

            Assert.IsTrue(userRepository.GetAll().Any());

            var newRepo = MyCore.Resolve<IRepository<AppUser>>();

            var userFromBase = newRepo.FindObject(x => x.ID == newUser.ID);
            var collUsersFrombase = newRepo.FindAll(x => x.ID == newUser.ID);

            Assert.AreEqual(1, collUsersFrombase.Count());

            var userFromColl = collUsersFrombase.First();
            var users = new AppUser[] { newUser, userFromColl, userFromBase };

            Assert.AreEqual(newUser.ID, userFromBase.ID);
            Assert.AreEqual(newUser.ID, userFromColl.ID);
            Assert.AreEqual(userFromColl.ID, userFromBase.ID);

            foreach (var user in users)
            {
                
                Assert.AreEqual(nick, user.Nick);
            }
        }

        [Test]
        public void CanDeleteUser()
        {

            var newUser = new AppUser() { Nick = "aa" };
            var userRepository = MyCore.Resolve<IRepository<AppUser>>();
            userRepository.Add(newUser);

            Assert.IsTrue(userRepository.GetAll().Any());

            var newRepo = MyCore.Resolve<IRepository<AppUser>>();
            newRepo.Delete(newUser.ID);

            var userfrombase = newRepo.FindObject(x => x.ID == newUser.ID);
            Assert.IsTrue(userfrombase == null);

            var newRepo2 = MyCore.Resolve<IRepository<AppUser>>();
            var userfrombase2 = newRepo2.FindObject(x => x.ID == newUser.ID);
            Assert.IsTrue(userfrombase2 == null);

        }

        [Test]
        public void CanUseObservableColl()
        {

            var student = new Student();
            var wydzial = new Wydzial();

            student.Wydzialy.Add(wydzial);
            var repo = MyCore.Resolve<IRepository<Student>>();
            repo.Add(student);

            var newRepo = MyCore.Resolve<IRepository<Student>>();
            var newStud = newRepo.FindObject(x => x.ID == student.ID);

            Assert.IsNotNull(newStud);

        }
    }
}

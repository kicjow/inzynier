﻿using System;
using System.Threading.Tasks;
using Bartek.DataAccess.Contract.Misc;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.Contracts;
using System.Security.Claims;

namespace Bartek.DataAccess.Contract.UserManagers
{
    public interface IUserManager<IMyIdentityUser> //where T : IBaseObject, IMyIdentityUser
    {
        //  IUserManager<T> Create(IOwinContext context);
        Task<IMyIdentityResult> CreateAsync(IMyIdentityUser user, string password);
        Task<IMyIdentityResult> ConfirmEmailAsync(IMyIdentityUser user, string token);
        Task<IMyIdentityUser> FindByNameAsync(IMyIdentityUser user);
        Task<bool> IsEmailConfirmedAsync(IMyIdentityUser user);
        Task<IMyIdentityResult> ResetPasswordAsync(IMyIdentityUser user, string token, string newPassword);
        Task<ClaimsIdentity> CreateIdentityAsync(IMyIdentityUser user, string authType);
        Task<IMyIdentityUser> FindByIdAsync(Guid id);
        IMyIdentityUser FindById(Guid key);
        Task<IMyIdentityResult> DeleteAsync(IMyIdentityUser user);
    }
}

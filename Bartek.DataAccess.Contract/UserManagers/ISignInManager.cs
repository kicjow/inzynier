﻿using Bartek.DataAccess.Contract.UserEnums;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Contract.IUserManagers
{
    public interface ISignInManager<IMyIdentityUser> //where T : IMyIdentityUser
    {
        Task<MySignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent);
        Task SignInAsync(IMyIdentityUser user, bool isPersistent, bool rememberBrowser);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Contract.Misc
{
    public interface IMyIdentityResult
    {
        IMyIdentityResult Success { get; }
        IEnumerable<string> Errors { get; }
        bool Succeeded { get; }
        IMyIdentityResult Failed(params string[] errors);
    }
}

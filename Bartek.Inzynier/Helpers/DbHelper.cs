﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.Core.Helpers
{
    public static class DbHelper
    {
        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                //if (System.Diagnostics.Debugger.IsAttached)
                //{
                //    return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                //}
                //else
                //{
                //    return ConfigurationManager.ConnectionStrings["ProdukcjaString"].ConnectionString;
                //}
            }
        }
    }
}

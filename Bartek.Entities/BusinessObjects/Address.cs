﻿namespace Bartek.Entities.BusinessObjects
{
    public class Address : BaseObject
    {
        public string Ulica { get; set; }
        public string KodPocztowy { get; set; }
        public string Miasto { get; set; }
        public string NumerDomu { get; set; }
        public string NumierMieszkania { get; set; }

    }
}
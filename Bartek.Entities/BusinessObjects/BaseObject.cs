﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using AssociationManager;

namespace Bartek.Entities.BusinessObjects
{
    public class BaseObject : Bindable, IBaseObject
    {
        public BaseObject()
        {
            this.ID = Guid.NewGuid();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.None), Key]
        public Guid ID { get; set; }


       
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class Student : Person
    {
        public Student()
        {
            GrupyZajeciowe = new ObservableCollection<GrupaZajeciowa>();
            StudentPrzedmiotEdycje = new ObservableCollection<StudentPrzedmiotEdycja>();
            Wydzialy = new ObservableCollection<Wydzial>();
            BindCollections();
        }

        public string NrIndeksu { get; set; }
        [BindAssociation("Student-Grupa")]
        public virtual ObservableCollection<GrupaZajeciowa> GrupyZajeciowe { get; set; }
        [BindAssociation("Student-PrzedmioEdycja")]
        public virtual ObservableCollection<StudentPrzedmiotEdycja> StudentPrzedmiotEdycje { get; set; }
        [BindAssociation("Student-Wydzial")]
        public virtual ObservableCollection<Wydzial> Wydzialy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class PracownikNaukowy : Person
    {
        public PracownikNaukowy()
        {
            PrzedmiotEdycjaGrupy = new ObservableCollection<PrzedmiotEdycjaGrupa>();
            BindCollections();
        }

        /// <summary>
        /// w koncu moze być też studentem
        /// </summary>
        public virtual Student Student { get; set; }
        [BindAssociation("Wykladowca-PrzedmiotEdycja")]
        public virtual ObservableCollection<PrzedmiotEdycjaGrupa> PrzedmiotEdycjaGrupy { get; set; }
        public virtual DaneWykladowcy DaneWykladowcy { get; set; }
        public virtual ICollection<Wydzial> Wydzialy { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class PracownikDziekanatu : Person
    {
        private Wydzial _wydzialPracy;

        [BindAssociation("Dziekanat-Wydzial")]
        public virtual Wydzial WydzialPracy
        {
            get { return _wydzialPracy; }
            set { SetProperty(value, ref _wydzialPracy); }
        }
    }
}

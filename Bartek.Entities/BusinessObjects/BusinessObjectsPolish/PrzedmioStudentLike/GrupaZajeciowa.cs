﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;
using Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class GrupaZajeciowa : BaseObject
    {
        private Wydzial _wydzial;

        public GrupaZajeciowa()
        {
            Studenci = new ObservableCollection<Student>();
            PrzedmiotEdycje = new List<PrzedmiotEdycja>();
            BindCollections();
        }
        [BindAssociation("Student-Grupa")]
        public virtual ObservableCollection<Student> Studenci { get; set; }

        public virtual ICollection<PrzedmiotEdycja> PrzedmiotEdycje { get; set; }
        public virtual Semestr SemestrUtworzenia { get; set; }
        public virtual Semestr AktualnySemestr { get; set; }

        [BindAssociation("Grupa-Wydzial")]
        public virtual Wydzial Wydzial
        {
            get { return _wydzial; }
            set { SetProperty(value, ref _wydzial); }
        }

        public string Nazwa { get; set; }

    }
}

﻿using Bartek.Entities.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;

namespace Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike
{
    public class StudentPrzedmiotEdycja : BaseObject
    {
       
        private Student _student;
        private PrzedmiotEdycjaGrupa _przedmiotEdycjaGrupa;

        public StudentPrzedmiotEdycja()
        {
            Oceny = new ObservableCollection<Ocena>();
            BindCollections();
        }
        [BindAssociation("Student-PrzedmioEdycja")]
        public virtual Student Student
        {
            get { return _student; }
            set { SetProperty(value, ref _student); }
        }

        [BindAssociation("StudentPrzedmiot-PrzedmiotEdycja")]
        public virtual PrzedmiotEdycjaGrupa PrzedmiotEdycjaGrupa
        {
            get { return _przedmiotEdycjaGrupa; }
            set { SetProperty(value,ref _przedmiotEdycjaGrupa); }
        }

        [BindAssociation("StudentPrzedmiot-Ocena")]
        public virtual ObservableCollection<Ocena> Oceny { get; set; }
    }
}

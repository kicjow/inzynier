﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class Ocena : BaseObject
    {
        private StudentPrzedmiotEdycja _studentPrzedmiotEdycja;

        public Ocena()
        {

        }

        [BindAssociation("StudentPrzedmiot-Ocena")]
        public virtual StudentPrzedmiotEdycja StudentPrzedmiotEdycja
        {
            get { return _studentPrzedmiotEdycja; }
            set { SetProperty(value, ref _studentPrzedmiotEdycja); }
        }

        public string Opis { get; set; }
        public virtual OcenaWartoscEnum WartoscEnum { get; set; }
    }

    public enum OcenaWartoscEnum
    {
        Niedopuszczajacy,
        Dostateczny,
        Dobry,
        BardzoDobry,
        Nieklasyfikowany
    }
}

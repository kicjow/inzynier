﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;

namespace Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike
{
    public class PrzedmiotEdycjaGrupa : BaseObject
    {

        private PracownikNaukowy _wykladowca;

        public PrzedmiotEdycjaGrupa()
        {
            StudentPrzedmiotEdycje = new ObservableCollection<StudentPrzedmiotEdycja>();
            Zalaczniki = new List<Zalacznik>();
            BindCollections();
        }

        [BindAssociation("Wykladowca-PrzedmiotEdycja")]
        public virtual PracownikNaukowy Wykladowca
        {
            get { return _wykladowca; }
            set { SetProperty(value, ref _wykladowca); }
        }

        [BindAssociation("StudentPrzedmiot-PrzedmiotEdycja")]
        public virtual ObservableCollection<StudentPrzedmiotEdycja> StudentPrzedmiotEdycje { get; set; }
       
        public virtual ICollection<Zalacznik> Zalaczniki { get; set; }
    }
}

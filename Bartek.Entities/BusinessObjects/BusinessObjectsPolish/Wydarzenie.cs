﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class Wydarzenie : BaseObject
    {

        public Wydarzenie()
        {
            Przedmiot = new ObservableCollection<PrzedmiotEdycjaBase>();
            BindCollections();
        }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public DateTime Data { get; set; }

        [BindAssociation("PrzedmiotEdycjaBase-Wydarzenie")]
        public virtual ObservableCollection<PrzedmiotEdycjaBase> Przedmiot { get; set; }
    }
}

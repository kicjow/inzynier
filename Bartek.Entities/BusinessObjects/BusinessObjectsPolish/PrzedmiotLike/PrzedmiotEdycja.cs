﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;

namespace Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike
{
    public class PrzedmiotEdycja : PrzedmiotEdycjaBase
    {
        public string DodatkoweInformacjeTylkoDlaPrzedmiotu { get; set; }
    }
}

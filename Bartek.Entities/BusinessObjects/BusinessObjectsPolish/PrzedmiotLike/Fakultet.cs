﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;

namespace Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike
{
    public class Fakultet : PrzedmiotBase
    {
        public Fakultet()
        {
            FakultetEdycje = new List<Edycja>();
        }
        public virtual IEnumerable<Edycja> FakultetEdycje { get; set; }
    }
}

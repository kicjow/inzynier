﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;

namespace Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike
{
    public abstract class PrzedmiotEdycjaBase : MyDictionary
    {
        private Przedmiot _przedmiot;

        public PrzedmiotEdycjaBase()
        {
            Wydarzenia = new ObservableCollection<Wydarzenie>();
            PrzedmiotEdycjaGrupy = new List<PrzedmiotEdycjaGrupa>();
            //Przedmioty = new List<PrzedmiotBase>();
            BindCollections();
        }
        public virtual Semestr SemestrEdycji { get; set; }

        [BindAssociation("Przedmiot-PrzedmiotEdycja")]
        public virtual Przedmiot Przedmiot
        {
            get { return _przedmiot; }
            set { SetProperty(value, ref _przedmiot); }
        }

        [BindAssociation("PrzedmiotEdycjaBase-Wydarzenie")]
        public virtual ObservableCollection<Wydarzenie> Wydarzenia { get; set; }
       

        public virtual ICollection<PrzedmiotEdycjaGrupa> PrzedmiotEdycjaGrupy { get; set; }

        public virtual PracownikNaukowy OpiekunEdycji { get; set; }
    }
}

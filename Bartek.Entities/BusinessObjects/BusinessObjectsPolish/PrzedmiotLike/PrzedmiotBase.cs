﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public abstract class PrzedmiotBase : MyDictionary
    {

        public PrzedmiotBase(bool isFakultet)
        {
            LataDostepnosci = new List<Semestr>();
            Fakultet = isFakultet;
        }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public virtual ICollection<Semestr> LataDostepnosci { get; set; }
        public bool Fakultet { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Entities.BusinessObjects;

namespace Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike
{
    public class Przedmiot : PrzedmiotBase
    {
        public Przedmiot(bool isFakultet) : base(isFakultet)
        {
            PrzedmiotEdycje = new ObservableCollection<PrzedmiotEdycjaBase>();
            BindCollections();
        }

        [BindAssociation("Przedmiot-PrzedmiotEdycja")]
        public virtual ObservableCollection<PrzedmiotEdycjaBase> PrzedmiotEdycje { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjects;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class Zalacznik : BaseObject
    {
        public string Nazwa { get; set; }
        public string Opis { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Core;
using Bartek.Entities.BusinessObjects;

namespace Bartek.Entities.BusinessObjectsPolish
{
    public class Wydzial : BaseObject
    {
        public Wydzial()
        {
            GrupaZajeciowe = new ObservableCollection<GrupaZajeciowa>();
            Studenci = new ObservableCollection<Student>();
            PracownikDziekanatu = new ObservableCollection<PracownikDziekanatu>();
            PracownikNaukowi = new List<PracownikNaukowy>();
            BindCollections();
        }
        public string Nazwa { get; set; }

        [BindAssociation("Grupa-Wydzial")]
        public virtual ObservableCollection<GrupaZajeciowa> GrupaZajeciowe { get; set; }

        [BindAssociation("Student-Wydzial")]
        public virtual ObservableCollection<Student> Studenci { get; set; }

        public virtual ICollection<PracownikNaukowy> PracownikNaukowi { get; set; }

        [BindAssociation("Dziekanat-Wydzial")]
        public virtual ObservableCollection<PracownikDziekanatu> PracownikDziekanatu { get; set; }


    }
}

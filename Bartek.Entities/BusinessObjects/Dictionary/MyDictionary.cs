﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.Entities.BusinessObjects
{
    public abstract class MyDictionary : BaseObject
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int Index { get; set; }      
    }
}

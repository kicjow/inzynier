﻿using Bartek.Entities.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.Entities.Contracts
{
    public interface IMyIdentityUser : IBaseObject
    {
        AppUser AppUser { get; set; }
        string UserName { get; set; }
        string Email { get; set; }

        T ConvertTo<T>() where T : class, IMyIdentityUser;
    }
}

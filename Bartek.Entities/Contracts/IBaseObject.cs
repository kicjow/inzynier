﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.Entities.BusinessObjects
{
    public interface IBaseObject
    {
        Guid ID { get; }
    }
}

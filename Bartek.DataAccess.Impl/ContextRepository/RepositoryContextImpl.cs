﻿using Bartek.Core.Helpers;
using Bartek.DataAccess.Impl.Ef.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Impl.Ef.ContextRepository
{
    public class RepositoryContextImpl : IRepositoryContext
    {
        private ApplicationContext _ApplicationContext;
        public ApplicationContext ApplicationContext
        {
            get
            {
                if (_ApplicationContext == null)
                {
                    string connectionString = DbHelper.ConnectionString;
                    _ApplicationContext = new ApplicationContext(connectionString);
                }

                return _ApplicationContext;
            }
        }
    }
}

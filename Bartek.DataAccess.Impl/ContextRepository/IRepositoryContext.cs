﻿using Bartek.DataAccess.Impl.Ef.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Impl.Ef.ContextRepository
{
    public interface IRepositoryContext
    {
        ApplicationContext ApplicationContext { get; }
    }
}

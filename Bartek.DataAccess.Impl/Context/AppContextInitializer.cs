﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.DataAccess.Impl.Ef.Context
{
    public class AppContextInitializer : CreateDatabaseIfNotExists<ApplicationContext>
    {
        public override void InitializeDatabase(ApplicationContext context)
        {
            context.Configuration.LazyLoadingEnabled = true;
            context.Configuration.ProxyCreationEnabled = true;

            base.InitializeDatabase(context); // podczas testów mi się te konteksty jebią
        }
        protected override void Seed(ApplicationContext context)
        {
            base.Seed(context);
        }
    }

    public sealed class Configuration : DbMigrationsConfiguration<ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}

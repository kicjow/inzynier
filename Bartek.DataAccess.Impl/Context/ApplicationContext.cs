﻿using Bartek.Core.Helpers;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.Configurations.EF.Configurations;
using Bartek.Entities.Confiugarations.EF.Configurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjectsPolish;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;
using Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike;

namespace Bartek.DataAccess.Impl.Ef.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<AppUser> Users { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PracownikDziekanatu> PracownicyDziekanatu { get; set; }
        public DbSet<PracownikNaukowy> PracownicyNaukowi { get; set; }
        public DbSet<Student> Studenci { get; set; }
        public DbSet<GrupaZajeciowa> GrupeZajeciowe { get; set; }
        public DbSet<PrzedmiotEdycjaGrupa> PrzedmiotEdycje { get; set; }
        public DbSet<StudentPrzedmiotEdycja> StudentPrzedmiotEdycje { get; set; }
        //public DbSet<Fakultet> Fakultety { get; set; }
        //public DbSet<Edycja> FakultetEdycje { get; set; }
        public DbSet<Semestr> Semestry { get; set; }
        public DbSet<Wydarzenie> Wydarzenia { get; set; }
        public DbSet<Wydzial> Wydzialy { get; set; }
        public DbSet<Zalacznik> Zalaczniki { get; set; }


        public ApplicationContext(string connectionString) : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            // Database.SetInitializer(new AppContextInitializer());
            //  bool czyUtworzyl = Database.CreateIfNotExists();

        }

        public DbSet GetDbSet<T>() where T : IBaseObject
        {
            DbSet result = null;
            var source = Activator.CreateInstance<T>();
            TypeSwitch.Do(source,
                TypeSwitch.Case<AppUser>(() => result = this.Users),
                TypeSwitch.Case<Person>(() => result = this.Persons),
                TypeSwitch.Case<PracownikDziekanatu>(() => result = this.PracownicyDziekanatu),
                TypeSwitch.Case<PracownikNaukowy>(() => result = this.PracownicyNaukowi),
                TypeSwitch.Case<Student>(() => result = this.Studenci),
                TypeSwitch.Case<GrupaZajeciowa>(() => result = this.GrupeZajeciowe),
                TypeSwitch.Case<PrzedmiotEdycjaGrupa>(() => result = this.PrzedmiotEdycje),
                TypeSwitch.Case<StudentPrzedmiotEdycja>(() => result = this.StudentPrzedmiotEdycje),
                //TypeSwitch.Case<Fakultet>(() => result = this.Fakultety),
                //TypeSwitch.Case<Edycja>(() => result = this.FakultetEdycje),
                TypeSwitch.Case<Semestr>(() => result = this.Semestry),
                TypeSwitch.Case<Wydarzenie>(() => result = this.Wydarzenia),
                TypeSwitch.Case<Wydzial>(() => result = this.Wydzialy),
                TypeSwitch.Case<Zalacznik>(() => result = this.Zalaczniki),

                TypeSwitch.Default(() => ThrowNewArgumentExeption(source)));

            return result;
        }

        private void ThrowNewArgumentExeption(object source)
        {
            throw new ArgumentException($"Zły typ został przekazy do GetDbSet<T>, ApplicationContext: {source.GetType()}");
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new AppUserConfiguration());
            modelBuilder.Configurations.Add(new PersonConfiguration());
        }
    }
}

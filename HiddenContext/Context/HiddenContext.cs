﻿using Bartek.Entities.BusinessObjects;
using Bartek.Entities.Configurations.EF.Configurations;
using Bartek.Entities.Confiugarations.EF.Configurations;
using Bartek.IdentitiDataAccess.Impl.EF;
using Bartek.IdentitiDataAccess.Impl.EF.Configurations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssociationManager;
using Bartek.Core.CastleCore;
using Bartek.Core.Helpers;
using Bartek.DataAccess.Contract.UserManagers;
using Bartek.Entities.BusinessObjectsPolish;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;
using Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike;
using Bartek.Entities.Contracts;
using HiddenContext.Seed;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HiddenContext
{
    public class HiddenContext : IdentityDbContext<MyIdentityUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public HiddenContext(string connString) : base(connString)
        {

            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<PracownikDziekanatu> PracownicyDziekanatu { get; set; }
        public DbSet<PracownikNaukowy> PracownicyNaukowi { get; set; }
        public DbSet<Student> Studenci { get; set; }
        public DbSet<GrupaZajeciowa> GrupeZajeciowe { get; set; }
        public DbSet<Przedmiot> Przedmiot { get; set; }
        public DbSet<PrzedmiotEdycja> PrzedmiotyEdycja { get; set; }
        public DbSet<PrzedmiotEdycjaGrupa> PrzedmiotEdycjeGrupy { get; set; }
        public DbSet<StudentPrzedmiotEdycja> StudentPrzedmiotEdycje { get; set; }
        //public DbSet<Fakultet> Fakultety { get; set; }
        //public DbSet<Edycja> FakultetEdycje { get; set; }
        public DbSet<Semestr> Semestry { get; set; }
        public DbSet<Wydarzenie> Wydarzenia { get; set; }
        public DbSet<Wydzial> Wydzialy { get; set; }
        public DbSet<Zalacznik> Zalaczniki { get; set; }

        //  public DbSet<MyIdentityUser> IdentUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new PersonConfiguration());
            modelBuilder.Configurations.Add(new AppUserConfiguration());
            modelBuilder.Configurations.Add(new MyIdentityUserConfiguration());
            modelBuilder.Configurations.Add(new PrzedmiotConfiguration());
            modelBuilder.Configurations.Add(new PrzedmiotEdycjaConfiguration());
            

            //modelBuilder.Configurations.Add(new GuidUserRoleConfiguration());
            //modelBuilder.Configurations.Add(new GuidUserClaimConfiguration());
            //modelBuilder.Configurations.Add(new GuidUserLoginConfiguration());
            //modelBuilder.Configurations.Add(new GuidRoleConfiguration());

            //modelBuilder.Entity<GuidRole>().HasKey(x => x.Id);
            //modelBuilder.Entity<GuidUserClaim>().HasKey(x => x.Id);
            //modelBuilder.Entity<GuidUserLogin>().HasKey(x => new { x.UserId, x.ProviderKey, x.LoginProvider });
            //modelBuilder.Entity<GuidUserRole>().HasKey(x => new { x.UserId, x.RoleId });


        }
    }


    public sealed class Configuration : DbMigrationsConfiguration<HiddenContext>
    {
        public Configuration()
        {
           
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(HiddenContext context)
        {
           
                // var userManager = MyCore.Resolve<IUserManager<IMyIdentityUser>>();
                var thisAssembly = typeof(Person).Assembly;
                var typesToRegister = thisAssembly.GetTypes();

                RelationshipContainer.CurrentApplicationName = "MyApp";
                var relationships = RelationshipContainer.AddNewContainer();
                RelationshipContainer.RegisterTypes(typesToRegister, relationships);


            var seedgen = new SeedGenerator();
            seedgen.BindSeed(context);
            //TODO claimy i role
            base.Seed(context);
           
        }

        public void SeedTest(HiddenContext context) //bastard injection
        {
            this.Seed(context);
        }
    }

    public class MyContextFactory : IDbContextFactory<HiddenContext>
    {
        public HiddenContext Create()
        {
            var connString = DbHelper.ConnectionString; //"Data Source=(local); Initial Catalog=Bartek-tests; Integrated Security=SSPI;";//ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            return new HiddenContext(connString);
        }
    }
}

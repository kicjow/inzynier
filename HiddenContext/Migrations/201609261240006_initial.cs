namespace HiddenContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppUsers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Nick = c.String(),
                        Person_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Person", t => t.Person_ID)
                .Index(t => t.Person_ID);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FistName = c.String(),
                        LastName = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        DaneWykladowcy_Opis = c.String(),
                        NrIndeksu = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Address_ID = c.Guid(),
                        Student_ID = c.Guid(),
                        WydzialPracy_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Address", t => t.Address_ID)
                .ForeignKey("dbo.Person", t => t.Student_ID)
                .ForeignKey("dbo.Wydzial", t => t.WydzialPracy_ID)
                .Index(t => t.Address_ID)
                .Index(t => t.Student_ID)
                .Index(t => t.WydzialPracy_ID);
            
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Ulica = c.String(),
                        KodPocztowy = c.String(),
                        Miasto = c.String(),
                        NumerDomu = c.String(),
                        NumierMieszkania = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Wydzial",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Nazwa = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.GrupaZajeciowa",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Nazwa = c.String(),
                        AktualnySemestr_ID = c.Guid(),
                        SemestrUtworzenia_ID = c.Guid(),
                        Wydzial_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Semestr", t => t.AktualnySemestr_ID)
                .ForeignKey("dbo.Semestr", t => t.SemestrUtworzenia_ID)
                .ForeignKey("dbo.Wydzial", t => t.Wydzial_ID)
                .Index(t => t.AktualnySemestr_ID)
                .Index(t => t.SemestrUtworzenia_ID)
                .Index(t => t.Wydzial_ID);
            
            CreateTable(
                "dbo.Semestr",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                        Index = c.Int(nullable: false),
                        PrzedmiotBase_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PrzedmiotBase", t => t.PrzedmiotBase_ID)
                .Index(t => t.PrzedmiotBase_ID);
            
            CreateTable(
                "dbo.PrzedmiotEdycjaBase",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                        Index = c.Int(nullable: false),
                        DodatkoweInformacjeTylkoDlaPrzedmiotu = c.String(),
                        OpiekunEdycji_ID = c.Guid(),
                        Przedmiot_ID = c.Guid(),
                        SemestrEdycji_ID = c.Guid(),
                        GrupaZajeciowa_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Person", t => t.OpiekunEdycji_ID)
                .ForeignKey("dbo.PrzedmiotBase", t => t.Przedmiot_ID)
                .ForeignKey("dbo.Semestr", t => t.SemestrEdycji_ID)
                .ForeignKey("dbo.GrupaZajeciowa", t => t.GrupaZajeciowa_ID)
                .Index(t => t.OpiekunEdycji_ID)
                .Index(t => t.Przedmiot_ID)
                .Index(t => t.SemestrEdycji_ID)
                .Index(t => t.GrupaZajeciowa_ID);
            
            CreateTable(
                "dbo.PrzedmiotEdycjaGrupa",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Wykladowca_ID = c.Guid(),
                        PrzedmiotEdycjaBase_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Person", t => t.Wykladowca_ID)
                .ForeignKey("dbo.PrzedmiotEdycjaBase", t => t.PrzedmiotEdycjaBase_ID)
                .Index(t => t.Wykladowca_ID)
                .Index(t => t.PrzedmiotEdycjaBase_ID);
            
            CreateTable(
                "dbo.StudentPrzedmiotEdycja",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PrzedmiotEdycjaGrupa_ID = c.Guid(),
                        Student_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PrzedmiotEdycjaGrupa", t => t.PrzedmiotEdycjaGrupa_ID)
                .ForeignKey("dbo.Person", t => t.Student_ID)
                .Index(t => t.PrzedmiotEdycjaGrupa_ID)
                .Index(t => t.Student_ID);
            
            CreateTable(
                "dbo.Ocena",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Opis = c.String(),
                        WartoscEnum = c.Int(nullable: false),
                        StudentPrzedmiotEdycja_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.StudentPrzedmiotEdycja", t => t.StudentPrzedmiotEdycja_ID)
                .Index(t => t.StudentPrzedmiotEdycja_ID);
            
            CreateTable(
                "dbo.Zalacznik",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Nazwa = c.String(),
                        Opis = c.String(),
                        PrzedmiotEdycjaGrupa_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PrzedmiotEdycjaGrupa", t => t.PrzedmiotEdycjaGrupa_ID)
                .Index(t => t.PrzedmiotEdycjaGrupa_ID);
            
            CreateTable(
                "dbo.PrzedmiotBase",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Nazwa = c.String(),
                        Opis = c.String(),
                        Fakultet = c.Boolean(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                        Index = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Wydarzenie",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Nazwa = c.String(),
                        Opis = c.String(),
                        Data = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                        MyIdentityUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.MyIdentityUser", t => t.MyIdentityUser_Id)
                .Index(t => t.RoleId)
                .Index(t => t.MyIdentityUser_Id);
            
            CreateTable(
                "dbo.MyIdentityUser",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                        AppUser_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.AppUser_ID)
                .Index(t => t.AppUser_ID);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        MyIdentityUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MyIdentityUser", t => t.MyIdentityUser_Id)
                .Index(t => t.MyIdentityUser_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                        MyIdentityUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.MyIdentityUser", t => t.MyIdentityUser_Id)
                .Index(t => t.MyIdentityUser_Id);
            
            CreateTable(
                "dbo.StudentGrupaZajeciowa",
                c => new
                    {
                        Student_ID = c.Guid(nullable: false),
                        GrupaZajeciowa_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Student_ID, t.GrupaZajeciowa_ID })
                .ForeignKey("dbo.Person", t => t.Student_ID, cascadeDelete: true)
                .ForeignKey("dbo.GrupaZajeciowa", t => t.GrupaZajeciowa_ID, cascadeDelete: true)
                .Index(t => t.Student_ID)
                .Index(t => t.GrupaZajeciowa_ID);
            
            CreateTable(
                "dbo.StudentWydzial",
                c => new
                    {
                        Student_ID = c.Guid(nullable: false),
                        Wydzial_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Student_ID, t.Wydzial_ID })
                .ForeignKey("dbo.Person", t => t.Student_ID, cascadeDelete: true)
                .ForeignKey("dbo.Wydzial", t => t.Wydzial_ID, cascadeDelete: true)
                .Index(t => t.Student_ID)
                .Index(t => t.Wydzial_ID);
            
            CreateTable(
                "dbo.PracownikNaukowyWydzial",
                c => new
                    {
                        PracownikNaukowy_ID = c.Guid(nullable: false),
                        Wydzial_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.PracownikNaukowy_ID, t.Wydzial_ID })
                .ForeignKey("dbo.Person", t => t.PracownikNaukowy_ID, cascadeDelete: true)
                .ForeignKey("dbo.Wydzial", t => t.Wydzial_ID, cascadeDelete: true)
                .Index(t => t.PracownikNaukowy_ID)
                .Index(t => t.Wydzial_ID);
            
            CreateTable(
                "dbo.WydarzeniePrzedmiotEdycjaBase",
                c => new
                    {
                        Wydarzenie_ID = c.Guid(nullable: false),
                        PrzedmiotEdycjaBase_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Wydarzenie_ID, t.PrzedmiotEdycjaBase_ID })
                .ForeignKey("dbo.Wydarzenie", t => t.Wydarzenie_ID, cascadeDelete: true)
                .ForeignKey("dbo.PrzedmiotEdycjaBase", t => t.PrzedmiotEdycjaBase_ID, cascadeDelete: true)
                .Index(t => t.Wydarzenie_ID)
                .Index(t => t.PrzedmiotEdycjaBase_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Semestr", "PrzedmiotBase_ID", "dbo.PrzedmiotBase");
            DropForeignKey("dbo.AspNetUserRoles", "MyIdentityUser_Id", "dbo.MyIdentityUser");
            DropForeignKey("dbo.AspNetUserLogins", "MyIdentityUser_Id", "dbo.MyIdentityUser");
            DropForeignKey("dbo.AspNetUserClaims", "MyIdentityUser_Id", "dbo.MyIdentityUser");
            DropForeignKey("dbo.MyIdentityUser", "AppUser_ID", "dbo.AppUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AppUsers", "Person_ID", "dbo.Person");
            DropForeignKey("dbo.Person", "WydzialPracy_ID", "dbo.Wydzial");
            DropForeignKey("dbo.GrupaZajeciowa", "Wydzial_ID", "dbo.Wydzial");
            DropForeignKey("dbo.GrupaZajeciowa", "SemestrUtworzenia_ID", "dbo.Semestr");
            DropForeignKey("dbo.PrzedmiotEdycjaBase", "GrupaZajeciowa_ID", "dbo.GrupaZajeciowa");
            DropForeignKey("dbo.WydarzeniePrzedmiotEdycjaBase", "PrzedmiotEdycjaBase_ID", "dbo.PrzedmiotEdycjaBase");
            DropForeignKey("dbo.WydarzeniePrzedmiotEdycjaBase", "Wydarzenie_ID", "dbo.Wydarzenie");
            DropForeignKey("dbo.PrzedmiotEdycjaBase", "SemestrEdycji_ID", "dbo.Semestr");
            DropForeignKey("dbo.PrzedmiotEdycjaGrupa", "PrzedmiotEdycjaBase_ID", "dbo.PrzedmiotEdycjaBase");
            DropForeignKey("dbo.PrzedmiotEdycjaBase", "Przedmiot_ID", "dbo.PrzedmiotBase");
            DropForeignKey("dbo.PrzedmiotEdycjaBase", "OpiekunEdycji_ID", "dbo.Person");
            DropForeignKey("dbo.PracownikNaukowyWydzial", "Wydzial_ID", "dbo.Wydzial");
            DropForeignKey("dbo.PracownikNaukowyWydzial", "PracownikNaukowy_ID", "dbo.Person");
            DropForeignKey("dbo.Person", "Student_ID", "dbo.Person");
            DropForeignKey("dbo.Zalacznik", "PrzedmiotEdycjaGrupa_ID", "dbo.PrzedmiotEdycjaGrupa");
            DropForeignKey("dbo.PrzedmiotEdycjaGrupa", "Wykladowca_ID", "dbo.Person");
            DropForeignKey("dbo.StudentWydzial", "Wydzial_ID", "dbo.Wydzial");
            DropForeignKey("dbo.StudentWydzial", "Student_ID", "dbo.Person");
            DropForeignKey("dbo.StudentPrzedmiotEdycja", "Student_ID", "dbo.Person");
            DropForeignKey("dbo.StudentGrupaZajeciowa", "GrupaZajeciowa_ID", "dbo.GrupaZajeciowa");
            DropForeignKey("dbo.StudentGrupaZajeciowa", "Student_ID", "dbo.Person");
            DropForeignKey("dbo.StudentPrzedmiotEdycja", "PrzedmiotEdycjaGrupa_ID", "dbo.PrzedmiotEdycjaGrupa");
            DropForeignKey("dbo.Ocena", "StudentPrzedmiotEdycja_ID", "dbo.StudentPrzedmiotEdycja");
            DropForeignKey("dbo.GrupaZajeciowa", "AktualnySemestr_ID", "dbo.Semestr");
            DropForeignKey("dbo.Person", "Address_ID", "dbo.Address");
            DropIndex("dbo.WydarzeniePrzedmiotEdycjaBase", new[] { "PrzedmiotEdycjaBase_ID" });
            DropIndex("dbo.WydarzeniePrzedmiotEdycjaBase", new[] { "Wydarzenie_ID" });
            DropIndex("dbo.PracownikNaukowyWydzial", new[] { "Wydzial_ID" });
            DropIndex("dbo.PracownikNaukowyWydzial", new[] { "PracownikNaukowy_ID" });
            DropIndex("dbo.StudentWydzial", new[] { "Wydzial_ID" });
            DropIndex("dbo.StudentWydzial", new[] { "Student_ID" });
            DropIndex("dbo.StudentGrupaZajeciowa", new[] { "GrupaZajeciowa_ID" });
            DropIndex("dbo.StudentGrupaZajeciowa", new[] { "Student_ID" });
            DropIndex("dbo.AspNetUserLogins", new[] { "MyIdentityUser_Id" });
            DropIndex("dbo.AspNetUserClaims", new[] { "MyIdentityUser_Id" });
            DropIndex("dbo.MyIdentityUser", new[] { "AppUser_ID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "MyIdentityUser_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Zalacznik", new[] { "PrzedmiotEdycjaGrupa_ID" });
            DropIndex("dbo.Ocena", new[] { "StudentPrzedmiotEdycja_ID" });
            DropIndex("dbo.StudentPrzedmiotEdycja", new[] { "Student_ID" });
            DropIndex("dbo.StudentPrzedmiotEdycja", new[] { "PrzedmiotEdycjaGrupa_ID" });
            DropIndex("dbo.PrzedmiotEdycjaGrupa", new[] { "PrzedmiotEdycjaBase_ID" });
            DropIndex("dbo.PrzedmiotEdycjaGrupa", new[] { "Wykladowca_ID" });
            DropIndex("dbo.PrzedmiotEdycjaBase", new[] { "GrupaZajeciowa_ID" });
            DropIndex("dbo.PrzedmiotEdycjaBase", new[] { "SemestrEdycji_ID" });
            DropIndex("dbo.PrzedmiotEdycjaBase", new[] { "Przedmiot_ID" });
            DropIndex("dbo.PrzedmiotEdycjaBase", new[] { "OpiekunEdycji_ID" });
            DropIndex("dbo.Semestr", new[] { "PrzedmiotBase_ID" });
            DropIndex("dbo.GrupaZajeciowa", new[] { "Wydzial_ID" });
            DropIndex("dbo.GrupaZajeciowa", new[] { "SemestrUtworzenia_ID" });
            DropIndex("dbo.GrupaZajeciowa", new[] { "AktualnySemestr_ID" });
            DropIndex("dbo.Person", new[] { "WydzialPracy_ID" });
            DropIndex("dbo.Person", new[] { "Student_ID" });
            DropIndex("dbo.Person", new[] { "Address_ID" });
            DropIndex("dbo.AppUsers", new[] { "Person_ID" });
            DropTable("dbo.WydarzeniePrzedmiotEdycjaBase");
            DropTable("dbo.PracownikNaukowyWydzial");
            DropTable("dbo.StudentWydzial");
            DropTable("dbo.StudentGrupaZajeciowa");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.MyIdentityUser");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Wydarzenie");
            DropTable("dbo.PrzedmiotBase");
            DropTable("dbo.Zalacznik");
            DropTable("dbo.Ocena");
            DropTable("dbo.StudentPrzedmiotEdycja");
            DropTable("dbo.PrzedmiotEdycjaGrupa");
            DropTable("dbo.PrzedmiotEdycjaBase");
            DropTable("dbo.Semestr");
            DropTable("dbo.GrupaZajeciowa");
            DropTable("dbo.Wydzial");
            DropTable("dbo.Address");
            DropTable("dbo.Person");
            DropTable("dbo.AppUsers");
        }
    }
}

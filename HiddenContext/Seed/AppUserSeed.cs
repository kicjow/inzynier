﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjects;

namespace HiddenContext.Seed
{
    public class AppUserSeed
    {
        public ICollection<AppUser> UsersSeed()
        {
            var user1 = new AppUser();
            user1.Nick = "bartek";

            var user2 = new AppUser();
            user1.Nick = "adam";

            var user3 = new AppUser();
            user1.Nick = "lucja";

            return new List<AppUser>() { user1, user2, user3 };

        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjectsPolish;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;
using Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike;

namespace HiddenContext.Seed
{
    public class WydzialSeed
    {
        public ICollection<Wydzial> Wydzialy()
        {
            var wydzial = new Wydzial();
            wydzial.Nazwa = "Informatyka";

            var wydzial2 = new Wydzial();
            wydzial2.Nazwa = "kosmetologia";

            return new List<Wydzial>(new Wydzial[] { wydzial2, wydzial });
        }

        public ICollection<GrupaZajeciowa> GrupyZajeciowe()
        {
            var isi = new GrupaZajeciowa();
            isi.Nazwa = "ISI1";
            //isi.AktualnySemestr = null; //TODO

            var kos = new GrupaZajeciowa();
            kos.Nazwa = "KOsmetologia1";
            //kos.AktualnySemestr = null;//TODO

            var result = new List<GrupaZajeciowa>();
            result.Add(isi);
            result.Add(kos);

            return result;
        }

        public ICollection<Przedmiot> Przedmioty()
        {
            var fak1 = new Przedmiot(true);
            //fak1.LataDostepnosci = null;//todo
            fak1.Nazwa = "bazy danych";
            fak1.Opis = "bazodanuejmy szybko";
            fak1.Value = "BAZA";
            fak1.Key = "B1";

            var fak2 = new Przedmiot(true);
            //fak2.LataDostepnosci = null;//todo
            fak2.Nazwa = "pazurki";
            fak2.Opis = "żelowe i tipsy";
            fak2.Value = "KOSM";
            fak2.Key = "K1";

            var przed1 = new Przedmiot(false);
            //przed1.LataDostepnosci = null;//todo
            przed1.Nazwa = "majca";
            przed1.Opis = "całkujemy i mnożymy";
            przed1.Value = "ANA1";
            przed1.Key = "A1";

            var przed2 = new Przedmiot(false);
            //przed2.LataDostepnosci = null;//todo
            przed2.Nazwa = "wuef";
            przed2.Opis = "biegamy i rzucamy młotem";
            przed2.Value = "WF";
            przed2.Key = "WF1";

            var result = new List<Przedmiot> { fak2, fak1, przed1, przed2 };
            return result;
        }

        public ICollection<PrzedmiotEdycja> EdycjePrzedmiotow()
        {
            var przedmioty = Przedmioty().ToList();
            var pracownicy = new PeopleSeed().PracownicyNaukowiSeed().ToList();

            var edycjaMatmy1 = new PrzedmiotEdycja();
            edycjaMatmy1.DodatkoweInformacjeTylkoDlaPrzedmiotu = "info";
            //edycjaMatmy1.Przedmiot = przedmioty.FirstOrDefault(x => x.Key == "A1");
            edycjaMatmy1.Key = "A12016/2017";
            edycjaMatmy1.Value = "ANAL1617";
            //edycjaMatmy1.OpiekunEdycji = pracownicy.FirstOrDefault();
            //edycjaMatmy1.PrzedmiotEdycjaGrupy = null;//todo
            //edycjaMatmy1.Wydarzenia = null;//todo
            //edycjaMatmy1.SemestrEdycji = null;//todo

            var edycjaMatmy2 = new PrzedmiotEdycja();
            edycjaMatmy2.DodatkoweInformacjeTylkoDlaPrzedmiotu = "info2";
            //edycjaMatmy2.Przedmiot = przedmioty.FirstOrDefault(x => x.Key == "A1");
            edycjaMatmy2.Key = "A12015/2016";
            edycjaMatmy2.Value = "ANAL1516";
            //edycjaMatmy2.OpiekunEdycji = pracownicy.FirstOrDefault();
            //edycjaMatmy2.PrzedmiotEdycjaGrupy = null;//todo
            //edycjaMatmy2.Wydarzenia = null;//todo
            //edycjaMatmy2.SemestrEdycji = null;//todo

            var edycjaPazurkow1 = new PrzedmiotEdycja();
            edycjaPazurkow1.DodatkoweInformacjeTylkoDlaPrzedmiotu = "info3";
            //edycjaPazurkow1.Przedmiot = przedmioty.FirstOrDefault(x => x.Key == "K1");
            edycjaPazurkow1.Key = "K12016/2017";
            edycjaPazurkow1.Value = "KOS1617";
            //edycjaPazurkow1.OpiekunEdycji = pracownicy[2];
            //edycjaPazurkow1.PrzedmiotEdycjaGrupy = null;//todo
            //edycjaPazurkow1.Wydarzenia = null;//todo
            //edycjaPazurkow1.SemestrEdycji = null;//todo

            var edycjaBazDanych = new PrzedmiotEdycja();
            edycjaBazDanych.DodatkoweInformacjeTylkoDlaPrzedmiotu = "info4";
            //edycjaBazDanych.Przedmiot = przedmioty.FirstOrDefault(x => x.Key == "B1");
            edycjaBazDanych.Key = "B12016/2017";
            edycjaBazDanych.Value = "BAZY1617";
            //edycjaBazDanych.OpiekunEdycji = pracownicy.FirstOrDefault();
            //edycjaBazDanych.PrzedmiotEdycjaGrupy = null;//todo
            //edycjaBazDanych.Wydarzenia = null;//todo
            //edycjaBazDanych.SemestrEdycji = null;//todo

            return new List<PrzedmiotEdycja>() { edycjaMatmy1, edycjaMatmy2, edycjaPazurkow1, edycjaBazDanych };
        }


        public ICollection<PrzedmiotEdycjaGrupa> PrzedmiotGrupy()
        {
            var grupa1 = new PrzedmiotEdycjaGrupa();
            var grupa2 = new PrzedmiotEdycjaGrupa();
            var grupa3 = new PrzedmiotEdycjaGrupa();

            return new List<PrzedmiotEdycjaGrupa>() { grupa1, grupa2, grupa3 };
        }

     

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjects;
using Bartek.Entities.BusinessObjectsPolish;

namespace HiddenContext.Seed
{
    public class PeopleSeed
    {
        public ICollection<Student> StudentsSeed()
        {
            var result = new List<Student>();

            var student1 = new Student
            {
                Address = new Address()
                {
                    KodPocztowy = "05-071",
                    Miasto = "Sulejówek",
                    NumerDomu = "4",
                    NumierMieszkania = "10",
                    Ulica = "asadów"
                },
                Email = "energetic.vex@gmail.com",
                FistName = "bunio",
                LastName = "wojcik",
                PhoneNumber = "796 555 444",
                NrIndeksu = "111111"
            };

            //student1.GrupyZajeciowe = null; //TODO
            //student1.StudentPrzedmiotEdycje = null; //todo
            //student1.Wydzialy = null; //todo

            var student2 = new Student
            {
                Address = new Address()
                {
                    KodPocztowy = "03371",
                    Miasto = "Łomża",
                    NumerDomu = "34",
                    NumierMieszkania = "103",
                    Ulica = "asd"
                },
                Email = "maisdm@gmail.com",
                FistName = "adam",
                LastName = "kącek",
                PhoneNumber = "2222 33 444"
            };

            //student2.GrupyZajeciowe = null; //TODO
            //student2.StudentPrzedmiotEdycje = null; //todo
            //student2.Wydzialy = null; //todo

            var student3 = new Student
            {
                Address = new Address()
                {
                    KodPocztowy = "56-233",
                    Miasto = "Warszawa",
                    NumerDomu = "14",
                    NumierMieszkania = "1",
                    Ulica = "miszków"
                },
                Email = "baba@o2.pl",
                FistName = "michał",
                LastName = "stępień",
                PhoneNumber = "3334445555"
            };

            var student4 = new Student
            {
                Address = new Address()
                {
                    KodPocztowy = "56-23344",
                    Miasto = "Warszaw44a",
                    NumerDomu = "1444",
                    NumierMieszkania = "144",
                    Ulica = "mis44zków"
                },
                Email = "baba@44o2.pl",
                FistName = "mic444hał",
                LastName = "stęp4444ień",
                PhoneNumber = "4443334445555"
            };
            var student5 = new Student
            {
                Address = new Address()
                {
                    KodPocztowy = "56-555555233",
                    Miasto = "W55555arszawa",
                    NumerDomu = "155554",
                    NumierMieszkania = "155",
                    Ulica = "mi55555szków"
                },
                Email = "baba@o2.pl5555",
                FistName = "michał555",
                LastName = "55stępień5555",
                PhoneNumber = "3334445555"
            };
            var student6 = new Student
            {
                Address = new Address()
                {
                    KodPocztowy = "56-23366666",
                    Miasto = "6666Warszawa",
                    NumerDomu = "6666",
                    NumierMieszkania = "6661",
                    Ulica = "miszk66666ów"
                },
                Email = "6666baba@o2.pl",
                FistName = "6666michał",
                LastName = "6666stępień",
                PhoneNumber = "66663334445555"
            };

            //student3.GrupyZajeciowe = null; //TODO
            //student3.StudentPrzedmiotEdycje = null; //todo
            //student3.Wydzialy = null; //todo

            result.Add(student1);
            result.Add(student2);
            result.Add(student3);
            result.Add(student4);
            result.Add(student5);
            result.Add(student6);


            return result;
        }

        public ICollection<PracownikNaukowy> PracownicyNaukowiSeed()
        {
            var pracownik1 = new PracownikNaukowy
            {
                DaneWykladowcy = new DaneWykladowcy() { Opis = "opis1" },
                Student = (StudentsSeed().ToList())[0],
                FistName = "profesro1",
                LastName = "nazwisko",
                PhoneNumber = "1232"
                //PrzedmiotEdycjaGrupy = null, /*TODO*/
            };

            var pracownik2 = new PracownikNaukowy
            {
                DaneWykladowcy = new DaneWykladowcy() { Opis = "opis2" },
                Student = null,
                FistName = "profesro2",
                LastName = "nazwisko2",
                PhoneNumber = "7777772"
                //PrzedmiotEdycjaGrupy = null, /*TODO*/

            };

            var pracownik3 = new PracownikNaukowy
            {
                DaneWykladowcy = new DaneWykladowcy() { Opis = "opis3" },
                Student = null,
                FistName = "profesro3333",
                LastName = "nazwisko3333",
                PhoneNumber = "123123333"
                //PrzedmiotEdycjaGrupy = null, /*TODO*/

            };

            var result = new List<PracownikNaukowy>();
            result.Add(pracownik1);
            result.Add(pracownik2);
            result.Add(pracownik3);
            return result;
        }

        public ICollection<PracownikDziekanatu> PracownikDziekanatuSeed()
        {

            var pracownik1 = new PracownikDziekanatu
            {
                FistName = "halinka",
                LastName = "halinowska",
                PhoneNumber = "12312313"
                //WydzialPracy = null /*TODO*/
            };

            var pracownik2 = new PracownikDziekanatu
            {

                FistName = "grażynka",
                LastName = "grążynśka",
                PhoneNumber = "12312313"
                //WydzialPracy = null /*TODO*/
            };

            return new List<PracownikDziekanatu>(new PracownikDziekanatu[] { pracownik1, pracownik2 });
        }


    }
}

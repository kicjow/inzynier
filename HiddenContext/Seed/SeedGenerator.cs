﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjectsPolish;
using Bartek.Entities.BusinessObjectsPolish.PrzedmioStudentLike;
using Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike;

namespace HiddenContext.Seed
{
    public class SeedGenerator
    {
        public SeedGenerator()
        {
            _peopleSeed = new PeopleSeed();
            _wydzialSeed = new WydzialSeed();
        }

        private WydzialSeed _wydzialSeed;
        private PeopleSeed _peopleSeed;

        public void BindSeed(HiddenContext context)
        {
            var ps = _peopleSeed;
            var ws = _wydzialSeed;
            var studenci = ps.StudentsSeed().ToList();
            var profesorowie = ps.PracownicyNaukowiSeed().ToList();
            var panieZDziekanatu = ps.PracownikDziekanatuSeed().ToList();
            var wydzialy = ws.Wydzialy().ToList();
            var grupyZajeciowe = ws.GrupyZajeciowe().ToList();
            var przedmioty = ws.Przedmioty().ToList();
            var edycjePrzedm = ws.EdycjePrzedmiotow().ToList();
            var edycjeGrupy = ws.PrzedmiotGrupy().ToList();
            var studentPrzedmiotEdycje = new List<StudentPrzedmiotEdycja>();
            var appUsers = new AppUserSeed().UsersSeed().ToList();


            var kosmetologia = wydzialy[0];
            var informatyka = wydzialy[1];
            var paniZDziekanatuKosmetologia = panieZDziekanatu[0];
            var paniZDziekanatuInformatyka = panieZDziekanatu[1];

            #region WYDZIALY
            // wydzial
            kosmetologia.PracownikDziekanatu.Add(paniZDziekanatuKosmetologia);
            informatyka.PracownikDziekanatu.Add(paniZDziekanatuInformatyka);

            kosmetologia.GrupaZajeciowe.Add(grupyZajeciowe[1]);
            informatyka.GrupaZajeciowe.Add(grupyZajeciowe[0]);

            var studenciInfy = new List<Student>();
            studenciInfy.Add(studenci[0]);
            studenciInfy.Add(studenci[1]);
            studenciInfy.Add(studenci[2]);

            var studenciKosmetologi = new List<Student>();
            studenciKosmetologi.Add(studenci[3]);
            studenciKosmetologi.Add(studenci[4]);
            studenciKosmetologi.Add(studenci[5]);

            kosmetologia.Studenci.Add(studenci[3]);
            kosmetologia.Studenci.Add(studenci[4]);
            kosmetologia.Studenci.Add(studenci[5]);
            informatyka.Studenci.Add(studenci[0]);
            informatyka.Studenci.Add(studenci[1]);
            informatyka.Studenci.Add(studenci[2]);

            kosmetologia.PracownikNaukowi.Add(profesorowie[0]);
            kosmetologia.PracownikNaukowi.Add(profesorowie[1]);
            informatyka.PracownikNaukowi.Add(profesorowie[1]);
            informatyka.PracownikNaukowi.Add(profesorowie[2]);

            // grupy zajęciowe
            var grupaZajKosmet = grupyZajeciowe[1];
            var grupaZajInf = grupyZajeciowe[0];

            grupaZajKosmet.Studenci.Add(studenci[3]);
            grupaZajKosmet.Studenci.Add(studenci[4]);
            grupaZajKosmet.Studenci.Add(studenci[5]);

            grupaZajInf.Studenci.Add(studenci[0]);
            grupaZajInf.Studenci.Add(studenci[1]);
            grupaZajInf.Studenci.Add(studenci[2]);

            grupaZajKosmet.PrzedmiotEdycje.Add(edycjePrzedm[2]);
            grupaZajInf.PrzedmiotEdycje.Add(edycjePrzedm[1]);

            grupaZajKosmet.AktualnySemestr = new Semestr()
            {
                Key = "2016/2017",
                Value = "2016 na 2017"

            };
            grupaZajInf.AktualnySemestr = new Semestr()
            {
                Key = "2016/2017",
                Value = "2016 na 2017"
            };

            grupaZajInf.SemestrUtworzenia = new Semestr()
            {
                Key = "rok temu",
                Value = "rok temu"
            };
            grupaZajKosmet.SemestrUtworzenia = new Semestr()
            {
                Key = "rok temu",
                Value = "rok temu"
            };

            //przedmioty NIE edycja
            var pazurki = przedmioty[0];
            var majca = przedmioty[2];

            pazurki.PrzedmiotEdycje.Add(edycjePrzedm[2]);
            majca.PrzedmiotEdycje.Add(edycjePrzedm[1]);

            //przedmioty edycje
            var majcaEdycja = edycjePrzedm[1];
            var pazurkiEdycja = edycjePrzedm[2];

            majcaEdycja.Wydarzenia.Add(new Wydarzenie() { Data = DateTime.Now, Nazwa = "Kartkowka" });
            majcaEdycja.Wydarzenia.Add(new Wydarzenie() { Data = DateTime.Now.AddDays(-1d), Nazwa = "Kartkowka wczoraj" });

            pazurkiEdycja.Wydarzenia.Add(new Wydarzenie() { Data = DateTime.Now, Nazwa = "Kartkowka z pazurki" });
            pazurkiEdycja.Wydarzenia.Add(new Wydarzenie() { Data = DateTime.Now.AddDays(-1d), Nazwa = "Kartkowka wczoraj z pazurkow" });

            pazurkiEdycja.SemestrEdycji = new Semestr()
            {
                Key = "rok temu",
                Value = "rok temu"
            };
            majcaEdycja.SemestrEdycji = new Semestr()
            {
                Key = "rok temu",
                Value = "rok temu"
            };

            var przedmiotEdcyjaGrupyKosmet = edycjeGrupy[0];
            var przedmiotEdycjaGrupyINf = edycjeGrupy[1];

            pazurkiEdycja.PrzedmiotEdycjaGrupy.Add(przedmiotEdcyjaGrupyKosmet);
            majcaEdycja.PrzedmiotEdycjaGrupy.Add(przedmiotEdycjaGrupyINf);

            majcaEdycja.OpiekunEdycji = profesorowie[1];
            pazurkiEdycja.OpiekunEdycji = profesorowie[0];

            przedmiotEdcyjaGrupyKosmet.Wykladowca = profesorowie[0];
            przedmiotEdycjaGrupyINf.Wykladowca = profesorowie[1];

            var studentPrzedmiotEdycja1 = new StudentPrzedmiotEdycja();
            var studentPrzedmiotEdycja2 = new StudentPrzedmiotEdycja();
            var studentPrzedmiotEdycja3 = new StudentPrzedmiotEdycja();
            var studentPrzedmiotEdycja4 = new StudentPrzedmiotEdycja();
            var studentPrzedmiotEdycja5 = new StudentPrzedmiotEdycja();
            var studentPrzedmiotEdycja6 = new StudentPrzedmiotEdycja();

            studentPrzedmiotEdycje.Add(studentPrzedmiotEdycja1);
            studentPrzedmiotEdycje.Add(studentPrzedmiotEdycja2);
            studentPrzedmiotEdycje.Add(studentPrzedmiotEdycja3);
            studentPrzedmiotEdycje.Add(studentPrzedmiotEdycja4);
            studentPrzedmiotEdycje.Add(studentPrzedmiotEdycja5);
            studentPrzedmiotEdycje.Add(studentPrzedmiotEdycja6);

            przedmiotEdcyjaGrupyKosmet.StudentPrzedmiotEdycje.Add(studentPrzedmiotEdycja1);
            przedmiotEdcyjaGrupyKosmet.StudentPrzedmiotEdycje.Add(studentPrzedmiotEdycja2);
            przedmiotEdcyjaGrupyKosmet.StudentPrzedmiotEdycje.Add(studentPrzedmiotEdycja3);

            przedmiotEdycjaGrupyINf.StudentPrzedmiotEdycje.Add(studentPrzedmiotEdycja4);
            przedmiotEdycjaGrupyINf.StudentPrzedmiotEdycje.Add(studentPrzedmiotEdycja5);
            przedmiotEdycjaGrupyINf.StudentPrzedmiotEdycje.Add(studentPrzedmiotEdycja6);

            przedmiotEdcyjaGrupyKosmet.Zalaczniki.Add(new Zalacznik() { Nazwa = "nazwa zal kosmet", Opis = "asd kos" });
            przedmiotEdycjaGrupyINf.Zalaczniki.Add(new Zalacznik() { Nazwa = "nazwa zal inf", Opis = "asd inf" });

            studentPrzedmiotEdycja1.Student = studenci[3];
            studentPrzedmiotEdycja2.Student = studenci[4];
            studentPrzedmiotEdycja3.Student = studenci[5];

            studentPrzedmiotEdycja4.Student = studenci[0];
            studentPrzedmiotEdycja5.Student = studenci[1];
            studentPrzedmiotEdycja6.Student = studenci[2];

            studentPrzedmiotEdycja1.Oceny.Add(new Ocena() { Opis = "za ewgzamin", WartoscEnum = OcenaWartoscEnum.BardzoDobry });
            studentPrzedmiotEdycja1.Oceny.Add(new Ocena() { Opis = "za ewgzamin", WartoscEnum = OcenaWartoscEnum.BardzoDobry });
            studentPrzedmiotEdycja2.Oceny.Add(new Ocena() { Opis = "za ewgzamin", WartoscEnum = OcenaWartoscEnum.Dobry });
            studentPrzedmiotEdycja3.Oceny.Add(new Ocena() { Opis = "za ewgzamin", WartoscEnum = OcenaWartoscEnum.Niedopuszczajacy });
            studentPrzedmiotEdycja4.Oceny.Add(new Ocena() { Opis = "za ewgzamin", WartoscEnum = OcenaWartoscEnum.Dobry });
            studentPrzedmiotEdycja5.Oceny.Add(new Ocena() { Opis = "za ewgzamin", WartoscEnum = OcenaWartoscEnum.Dostateczny });
            studentPrzedmiotEdycja6.Oceny.Add(new Ocena() { Opis = "za ewgzamin", WartoscEnum = OcenaWartoscEnum.BardzoDobry });
            #endregion
            //uprawnienia

            var bartek = appUsers[0];
            var adam = appUsers[1];
            var lucja = appUsers[2];

            bartek.Person = profesorowie[0];
            adam.Person = studenci[0];
            lucja.Person = studenci[1];

            context.PrzedmiotyEdycja.AddRange(edycjePrzedm);
            context.Studenci.AddRange(studenci);
            context.PracownicyNaukowi.AddRange(profesorowie);
            context.PracownicyDziekanatu.AddRange(panieZDziekanatu);
            context.AppUsers.AddRange(appUsers);
            context.GrupeZajeciowe.AddRange(grupyZajeciowe);
            context.PrzedmiotEdycjeGrupy.AddRange(edycjeGrupy);
            context.StudentPrzedmiotEdycje.AddRange(studentPrzedmiotEdycje);
            context.Wydzialy.AddRange(wydzialy);
            context.Przedmiot.AddRange(przedmioty);
            

            context.SaveChanges();
        }
    }
}

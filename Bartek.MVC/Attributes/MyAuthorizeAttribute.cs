﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bartek.IdentitiDataAccess.Impl.EF.UserManagers;
using Bartek.MVC.Attributes;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Bartek.Core.CastleCore;
using Bartek.Entities.Contracts;
using Bartek.DataAccess.Contract.UserManagers;

namespace Bartek.MVC.Attributes
{
    public class MyAuthorizeAttribute : AuthorizeAttribute
    {

        public MyAuthorizeAttribute()
        {

        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            bool isAuth = Auth(httpContext);
            return base.AuthorizeCore(httpContext);
        }

        private bool Auth(HttpContextBase httpContext)
        {
            var key = httpContext.User.Identity.GetUserId();
            if (key == null)
            {
                return false;
            }
            var manager = MyCore.Resolve<IUserManager<IMyIdentityUser>>(); //httpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(Guid.Parse(key));

            return user != null;
        }
    }
}
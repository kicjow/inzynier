﻿using Bartek.Core.CastleCore;
using Bartek.DataAccess.Contract.UserManagers;
using Bartek.DataAccess.Impl.Ef.Context;
using Bartek.Entities.Contracts;
using Bartek.IdentitiDataAccess.Impl.EF;
using Bartek.IdentitiDataAccess.Impl.EF.UserManagers;
using Bartek.MVC.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;

[assembly: OwinStartupAttribute(typeof(OwinStartUp.Startup))]
namespace OwinStartUp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //antywzorzec, service locator
            app.CreatePerOwinContext(DbOwinHelper.CreateDbContext);
            app.CreatePerOwinContext<ApplicationUserManager>(DbOwinHelper.CreateUserManager);
            app.CreatePerOwinContext<ApplicationSignInManager>(DbOwinHelper.CreateSignInManager);                       
            
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,

                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, MyIdentityUser, Guid>(
             validateInterval: TimeSpan.FromMinutes(30),
             regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
             getUserIdCallback: (user) => Guid.Parse(user.GetUserId()))
                }
            });
        }
    }
}

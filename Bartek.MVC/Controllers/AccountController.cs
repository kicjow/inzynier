﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Bartek.Core.CastleCore;
using Bartek.DataAccess.Contract.IUserManagers;
using Bartek.DataAccess.Contract.Misc;
using Bartek.DataAccess.Contract.UserEnums;
using Bartek.DataAccess.Contract.UserManagers;
using Bartek.Entities.Contracts;
using Bartek.IdentitiDataAccess.Impl.EF.Misc;
using Bartek.MVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Bartek.Entities.BusinessObjects;
using Bartek.IdentitiDataAccess.Impl.EF;
using Bartek.MVC.Models.Account;
using Microsoft.Owin.Security;
using Bartek.IdentitiDataAccess.Impl.EF.UserManagers;

namespace Bartek.MVC.Controllers
{
    public partial class AccountController : BaseController
    {
        private ISignInManager<IMyIdentityUser> _signInManager;
        private IUserManager<IMyIdentityUser> _userManager;
        private ApplicationSignInManager _applicationSignInManagerFromOwin => HttpContext.GetOwinContext().Get<ApplicationSignInManager>();

        public AccountController(IUserManager<IMyIdentityUser> userManager, ISignInManager<IMyIdentityUser> signInManager)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
        }
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = MyCore.Resolve<IMyIdentityUser>();
                user.UserName = model.Email;
                user.Email = model.Email;
                var appuser = new AppUser();
                user.AppUser = appuser;

                var userManager = _userManager;
              

                var result = await userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var signInManager = _signInManager;

                    await signInManager.SignInAsync(user, false, false);
                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }


            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {


            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var signInManager = _applicationSignInManagerFromOwin;
            var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe);

            //switch (result)
            //{
            //    case MySignInStatus.Success:
            //        return RedirectToLocal(returnUrl);
            //    case MySignInStatus.Failure:
            //    default:
            //        ModelState.AddModelError("", "Invalid login attempt.");
            //        return View(model);
            //}

            //tu mozna by te dict jakoś injektować

            var redirectToLocal = new RedirectToLocalControllerCommand(returnUrl, this);
            var defaultCommand = new DefaultBehaviorControllerCommand("", "Invalid login attempt.", this, model);

            var dict = new Dictionary<MySignInStatus, IControllerCommand>
            {
                { MySignInStatus.Success,  redirectToLocal},
                { MySignInStatus.Failure, defaultCommand}
            };

            var handler = new ActionResultHandler(dict);
            return handler.DoCommand(result);

        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            var authManager = MyCore.Resolve<IAuthenticationManager>();
            authManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }


        private void AddErrors(IMyIdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

    }
}
﻿using Bartek.Core.CastleCore;
using Bartek.DataAccess.Contract.UserManagers;
using Bartek.Entities.Contracts;
using Bartek.IdentitiDataAccess.Impl.EF.UserManagers;
using Bartek.MVC.Attributes;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Bartek.MVC.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
           
        }

      
        [MyAuthorizeAttribute]
        public async Task<ActionResult> Contact()
        {
            var key = User.Identity.GetUserId();
            var manager = MyCore.Resolve<IUserManager<IMyIdentityUser>>();
            //TODO W TYM MIEJSCU MAM SERVICE LOCATOR, A POWINIENE MIEC DI
            //TODO W KONSTRUKTORZE POWINIENEM MIEC PRZEKAZYWANY MENAGER I TU GO TYLKO WYWOLYWAC
            var user = await manager.FindByIdAsync(Guid.Parse(key));
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
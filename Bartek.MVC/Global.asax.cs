﻿using Bartek.Core.CastleCore;
using Bartek.Core.Helpers;
using Bartek.DataAccess.Contract.IRepository;
using Bartek.DataAccess.Contract.IUserManagers;
using Bartek.DataAccess.Contract.UserManagers;
using Bartek.DataAccess.Impl.Ef.Context;
using Bartek.DataAccess.Impl.Ef.ContextRepository;
using Bartek.DataAccess.Impl.Ef.Repository;
using Bartek.Entities.Contracts;
using Bartek.IdentitiDataAccess.Impl.EF;
using Bartek.IdentitiDataAccess.Impl.EF.AuthenticationMangerFactory;
using Bartek.IdentitiDataAccess.Impl.EF.UserManagers;
using Bartek.MVC.Factories;
using Castle.MicroKernel.Registration;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AssociationManager;
using Bartek.Entities.BusinessObjects;
using Bartek.MVC.Infrastructure;

namespace Bartek.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterContainer();
        }

        private void RegisterContainer()
        {
            RootRegisterHelper.RegisterRelation();

            var connectionString = DbHelper.ConnectionString;

            MyCore.Container.Register(Component.For<IMyIdentityUser>().ImplementedBy<MyIdentityUser>());
            MyCore.Container.Register(Component.For<IAuthenticationMangerFactory>().ImplementedBy(typeof(AuthenticationMangerFactory)).LifestylePerWebRequest());
            MyCore.Container.Register(Component.For<IAuthenticationManager>().UsingFactoryMethod(AuthenticationMangerFactory.CreateAuthManagerStatic));
           // MyCore.Container.Register(Component.For<IOwinContext>().UsingFactoryMethod(AuthenticationMangerFactory.GetOwinContext));

            MyCore.Container.Register(Component.For<IUserManager<IMyIdentityUser>>().ImplementedBy<ApplicationUserManager>()
              .DependsOn(Dependency.OnValue(typeof(DbContext), new MyIdentityDbContext(connectionString))).LifestylePerWebRequest());

            // MyCore.Container.Register(Component.For<IUserManager<IMyIdentityUser>>().ImplementedBy<ApplicationUserManager>().LifestylePerWebRequest());
            MyCore.Container.Register(Component.For<ISignInManager<IMyIdentityUser>>().ImplementedBy<ApplicationSignInManager>().LifestylePerWebRequest());
            MyCore.Container.Register(Component.For<IRepositoryContext>().ImplementedBy<RepositoryContextImpl>().LifestylePerWebRequest());
            MyCore.Container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)).LifestylePerWebRequest());

            MyCore.Container.Install(new ApplicationCastleInstaller());
            var castleControllerFactory = new WindsorControllerFactory(MyCore.Container);

            ControllerBuilder.Current.SetControllerFactory(castleControllerFactory);
        }

        
    }
}

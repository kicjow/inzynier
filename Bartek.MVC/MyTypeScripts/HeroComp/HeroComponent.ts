﻿import { Component, Input, OnInit } from 'angular2/core';
import { Hero } from './Hero';
import { HeroDetailComponent } from './HeroDetailComponent'
import {HeroService} from './HeroService'
import Interfaces = require("../../node_modules/angular2/src/core/linker/interfaces");
const HEROES: Hero[] =
    [
        { id: 11, name: 'Mr. Nice' },
        { id: 12, name: 'Narco' },
        { id: 13, name: 'Bombasto' },
        { id: 14, name: 'Celeritas' },
        { id: 15, name: 'Magneta' },
        { id: 16, name: 'RubberMan' },
        { id: 17, name: 'Dynama' },
        { id: 18, name: 'Dr IQ' },
        { id: 19, name: 'Magma' },
        { id: 20, name: 'Tornado' }
    ];

@Component({
    selector: 'hero-app',
    templateUrl: 'MyTypeScripts/HeroComp/HeroTemplate.html',
    styles: [`
    .selected {
      background-color: #CFD8DC !important;
      color: white;
    }
    .heroes {
      margin: 0 0 2em 0;
      list-style-type: none;
      padding: 0;
      width: 15em;
    }
    .heroes li {
      cursor: pointer;
      position: relative;
      left: 0;
      background-color: #EEE;
      margin: .5em;
      padding: .3em 0;
      height: 1.6em;
      border-radius: 4px;
    }
    .heroes li.selected:hover {
      background-color: #BBD8DC !important;
      color: white;
    }
    .heroes li:hover {
      color: #607D8B;
      background-color: #DDD;
      left: .1em;
    }
    .heroes .text {
      position: relative;
      top: -3px;
    }
    .heroes .badge {
      display: inline-block;
      font-size: small;
      color: white;
      padding: 0.8em 0.7em 0 0.7em;
      background-color: #607D8B;
      line-height: 1em;
      position: relative;
      left: -1px;
      top: -4px;
      height: 1.8em;
      margin-right: .8em;
      border-radius: 4px 0 0 4px;
    }
  `],
    directives: [HeroDetailComponent],
    providers: [HeroService]
})
export class HeroComponent implements OnInit {

    constructor(private heroService: HeroService) {
    }

    ngOnInit() { 
        this.heroService.getHeroes().then(heroes => this.heroes = heroes);
    }

    title = 'Tour of Heroes';
    public heroes;

    myHero: Hero;


    onSelect(hero: Hero) { this.myHero = hero; }
}
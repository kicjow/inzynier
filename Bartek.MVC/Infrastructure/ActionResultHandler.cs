﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bartek.DataAccess.Contract.UserEnums;
using Bartek.MVC.Controllers;
using Bartek.MVC.Models.Account;

namespace Bartek.MVC.Controllers
{
    public partial class AccountController
    {
        private interface IControllerCommand
        {
            ActionResult Execute();
        }

        private class ActionResultHandler
        {
            private Dictionary<MySignInStatus, IControllerCommand> _dict;

            public ActionResultHandler(Dictionary<MySignInStatus, IControllerCommand> dict)
            {
                _dict = dict;
            }

            public ActionResult DoCommand(MySignInStatus status)
            {
                if (!_dict.ContainsKey(status))
                {
                    throw  new ArgumentException("brak odpowiedniego handlera");
                }

                return _dict[status].Execute();
            }
        }



        private class RedirectToLocalControllerCommand : IControllerCommand
        {
            private AccountController _controller;
            private readonly string _returnUrl;

            public RedirectToLocalControllerCommand(string returnUrl, AccountController controller)
            {
                _returnUrl = returnUrl;
                _controller = controller;
            }

            public ActionResult Execute()
            {
                return _controller.RedirectToLocal(_returnUrl);
            }
        }

        private class DefaultBehaviorControllerCommand : IControllerCommand
        {
            private readonly string _key;
            private readonly string _value;
            private readonly AccountController _controller;
            private readonly LoginViewModel _model;

            public DefaultBehaviorControllerCommand(string key, string value, AccountController controller, LoginViewModel model)
            {
                _key = key;
                _value = value;
                _controller = controller;
                _model = model;
            }

            public ActionResult Execute()
            {
                _controller.ModelState.AddModelError(_key, _value);
                return _controller.View(_model); //resharper tu coś odwala bo mysli, że jest w kontrolerze
            }
        }
    }
}
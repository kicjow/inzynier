﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssociationManager;
using Bartek.Entities.BusinessObjects;

namespace Bartek.MVC.Infrastructure
{
    public static class RootRegisterHelper
    {
        public static void RegisterRelation()
        {
            var thisAssembly = typeof(Person).Assembly;
            var typesToRegister = thisAssembly.GetTypes();

            RelationshipContainer.CurrentApplicationName = "MyApp";
            var relationships = RelationshipContainer.AddNewContainer();
            RelationshipContainer.RegisterTypes(typesToRegister, relationships);
        }
    }
}
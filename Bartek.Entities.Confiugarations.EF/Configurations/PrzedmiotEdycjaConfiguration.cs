﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bartek.Entities.BusinessObjectsPolish;
using Bartek.Entities.BusinessObjectsPolish.PrzedmiotLike;

namespace Bartek.Entities.Confiugarations.EF.Configurations
{
    public class PrzedmiotConfiguration : EntityTypeConfiguration<PrzedmiotBase>
    {
        public PrzedmiotConfiguration()
        {
            this.Map(x => x.MapInheritedProperties());
        }
    }

    public class PrzedmiotEdycjaConfiguration : EntityTypeConfiguration<PrzedmiotEdycja>
    {
        public PrzedmiotEdycjaConfiguration()
        {
            this.Map(x => x.MapInheritedProperties());
        }
    }
}

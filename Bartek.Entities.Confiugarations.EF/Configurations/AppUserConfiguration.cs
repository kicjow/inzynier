﻿using Bartek.Entities.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.Entities.Confiugarations.EF.Configurations
{
    public class AppUserConfiguration : EntityTypeConfiguration<AppUser>
    {
        public AppUserConfiguration()
        {
           // Property(x => x.Nick).IsRequired();
            //this.Map(x => x.MapInheritedProperties());
            this.ToTable("AppUsers");
        }
    }
}

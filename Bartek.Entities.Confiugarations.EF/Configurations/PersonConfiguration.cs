﻿using Bartek.Entities.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bartek.Entities.Configurations.EF.Configurations
{
    public class PersonConfiguration : EntityTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            //Property(x => x.FistName).IsRequired();
            //Property(x => x.ID).IsRequired();
          //  Map(x => x.MapInheritedProperties());
            HasKey(x => x.ID);
        }
    }
}
